<?php

defined('BASEPATH') or exit('NO direct script acces allowed');
class Carcarestore_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function create_carcarestore($savedata)
    {
        $data = array(
            'userid_carcarestore' => $savedata['userid_carcarestore'],
            'name_carcarestore' => $savedata['name_carcarestore'],
            'img' => $savedata['img'],
            'name' => $savedata['name'],
            'lastname' => $savedata['lastname'],
            'tel' => $savedata['tel'],
            'email' => $savedata['email'],
            'pass' => $savedata['pass'],
            'lat' => $savedata['lat'],
            'lng' => $savedata['lng'],
            'address' => $savedata['address'],
            'date' => $savedata['date'],
            'url_store' => $savedata['url_store'],
            'bank_number' => $savedata['bank_number'],
            'bank_bank' => $savedata['bank_bank'],
            'name_bank' => $savedata['name_bank'],
            'img_bank' => $savedata['img_bank']

        );
        $this->db->insert('carcarestore', $data);
        return $this->db->insert_id();
    }
    public function read_bank_all()
    {
        $this->db->select('name_bank,id_bank')->from('bank');
        $query = $this->db->get();
        return $query->result();
    }
    public function read_member_by_email_and_pass($email, $pass)
    {
        $where = array(
            'email' => $email,
            'pass' => $pass
        );
        $this->db->select('id_carcare,email')->from('carcarestore')->where($where);
        $qurey = $this->db->get();
        return $qurey->result();
    }

    public function read_carcarestore_by_id($userid_carcarestore)
    {
        $where = array(
            'userid_carcarestore' => $userid_carcarestore,
        );
        $this->db->select('id_carcare,name_carcarestore,img,name,lastname,tel,email,pass,address,userid_carcarestore,date')->from('carcarestore')->where($where);
        $query = $this->db->get();
        return $query->row();
    }
    public function getlocation($lng,$lat,$address)
    {   
        $sql ="SELECT name_carcarestore,lat,lng,address,tel,url_store
        FROM appmoro_boteye.carcarestore
        ORDER BY ((lat-$lat)*(lat-$lat)) + ((lng - $lng)*(lng - $lng)) ASC limit 5";
        $query = $this->db->query($sql);
        $this->queryResult = $query->result_array();
        $this->rows = count($this->queryResult);
        
        for ($i=0; $i<=$this->rows-1; $i++) {
            $lat2 = $this->queryResult[$i]['lat'];
            $lng2 = $this->queryResult[$i]['lng'];
            $distance = $this->distanceLatLng($lat,$lng,$lat2,$lng2);
            $this->queryResult[$i]["kilometers"] = $distance;
        }
        foreach($this->queryResult as $key => $value) {
            $this->queryResult[$key] = [
                'name' => $value['name_carcarestore'],
                'lat' => $value['lat'],
                'lng' => $value['lng'],
                'address' => $value['address'],
                'tel' => $value['tel'],
                'url_store' => $value['url_store'],
                'kilometers' => $this->distanceLatLng($lat,$lng,$value['lat'],$value['lng'])
            ];
        }
    
        //file_put_contents('log.txt', "query : " . print_r($this->queryResult,true) . PHP_EOL, FILE_APPEND);
        return $this->queryResult;
    }
    function distanceLatLng($lat="", $lng="", $lat2="",$lng2=""){
        $distance = 0;
        if($lat && $lng)
        {
            if($lat2 && $lng2)
            {
                $theta = (float) $lng - (float) $lng2;
                $dist = sin(deg2rad((float) $lat)) * sin(deg2rad((float) $lat2)) +  cos(deg2rad((float) $lat)) * cos(deg2rad((float) $lat2)) * cos(deg2rad((float) $theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $distance = $miles * 1.609344;
                $distance = round($distance, 2);
                $distance = (is_nan($distance) || is_infinite($distance))? 0 :$distance;
            }
        }
        return $distance;
    }

}
