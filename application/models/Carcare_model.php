<?php

defined('BASEPATH') OR exit('NO direct script acces allowed');

class Carcare_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function create_carcare($data) {
        $data = array(
            'userid' => $data['userid'],
            'name' => $data['name'],
            'type' => $data['type'],
            'carnumber' => $data['carnumber'],
            'option' => $data['option'],
            'price' => $data['price'],
            'date' => date("Y/m/d H:i:s"),
            'receipt' => 0
        );
//          file_put_contents('log.txt', "data : " . print_r($data, true) . PHP_EOL, FILE_APPEND);
        $this->db->insert('carcare', $data);
        return $this->db->insert_id();
    }
    public function read_carcare_by_id($userid) {
        $where = array(
            'userid' => $userid
        );
        $this->db->select('queue_carcare,date,name,carnumber,type,option,price,receipt')->from('carcare')->where($where);
        $query = $this->db->get();
        //file_put_contents('log.txt', "query : " . print_r($query,true) . PHP_EOL, FILE_APPEND);
        return $query->result();
        
    } 
    public function read_carcare_by_que($userid,$queue_carcare) {
        $where = array(
            'userid' => $userid,
            'queue_carcare' => $queue_carcare
        );
        $this->db->select('queue_carcare,date,name,carnumber,type,option,price,receipt')->from('carcare')->where($where);
        $query = $this->db->get();
        //file_put_contents('log.txt', "query : " . print_r($query,true) . PHP_EOL, FILE_APPEND);
        return $query->result();
        
    } 
    public function update_carcare($queue_carcare) {
        $this->db->set('receipt', 1);
        $this->db->where('queue_carcare', $queue_carcare);
        $this->db->update('carcare');
        return $this->db->affected_rows();
    }
}
