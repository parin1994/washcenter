<?php

defined('BASEPATH') OR exit('NO direct script acces allowed');

class Storeall_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function read_storeall_all() {
        $this->db->select('name_carcarestore,tel,address')->from('carcarestore');
        $query = $this->db->get();
        return $query->result();
    }
    public function search($key) {
        $where = array(
            'name_carcarestore' => $key
        );
        $this->db->select('name_carcarestore,tel,address')->from('carcarestore')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
}