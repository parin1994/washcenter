<?php

defined('BASEPATH') OR exit('NO direct script acces allowed');

class Price_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function read_price_by_id($type_price) {
        $where = array(
            'type_price' => $type_price,
        );
        $this->db->select('id_price,type_price,price1,price2,price3')->from('price')->where($where);
        $query = $this->db->get();
        return $query->row();
    }

}
