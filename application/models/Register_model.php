<?php

defined('BASEPATH') OR exit('NO direct script acces allowed');

class Register_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function create_register($savedata) {
        $data = array(
            'userid' => $savedata['userid'],
            'name_register' => $savedata['name_register'],
            'lastname_register' => $savedata['lastname_register'],
            'email_register' => $savedata['email_register'],
            'phone_register' => $savedata['phone_register'],
            'address_register' => $savedata['address_register'],
            'date_register' => $savedata['date_register']
        );

        $this->db->insert('register', $data);
        return $this->db->insert_id();
    }

    public function read_register_all() {
        $this->db->select('id_register,userid,name_register,lastname_register,email_register,phone_register,address_register,date_register')->from('register');
        $query = $this->db->get();
        return $query->result();
    }

    public function read_register_by_id($userid) {
        $where = array(
            'userid' => $userid,
        );
        $this->db->select('id_register,userid,name_register,lastname_register,email_register,phone_register,address_register,date_register')->from('register')->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function update_register($savedata) {
        $data = array(
            'id_register' => $savedata['id_register'],
            'userid' => $savedata['userid'],
            'name_register' => $savedata['name_register'],
            'lastname_register' => $savedata['lastname_register'],
            'email_register' => $savedata['email_register'],
            'phone_register' => $savedata['phone_register'],
            'address_register' => $savedata['address_register'],
            'date_register' => $savedata['date_register']
            
        );
        
        $this->db->where('id_register', $savedata['id_register']);
        $this->db->update('register', $data);
    }

}
