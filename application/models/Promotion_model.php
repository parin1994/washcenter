<?php

defined('BASEPATH') or exit('NO direct script acces allowed');

class Promotion_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function create_promotion($savedata)
    {
        $data = array(
            'name_pro' => $savedata['name_pro'],
            'option_pro' => $savedata['option_pro'],
            'price_pro' => $savedata['price_pro'],
            'date_pro' => $savedata['date_pro'],
            'date_out' => $savedata['date_out'],
            'email' => $savedata['email'],

        );

        $this->db->insert('promotion', $data);
        return $this->db->insert_id();
    }

    public function read_promotion_by_id($email)
    {
        $where = array(
            'email' => $email,
        );
        $this->db->select('id_promotion,name_pro,option_pro,price_pro,date_pro,date_out,email')->from('promotion')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_promotion_by_id1($id_promotion)
    {
        $where = array(
            'id_promotion' => $id_promotion,
        );
        $this->db->select('id_promotion,name_pro,option_pro,price_pro,date_pro,date_out,email')->from('promotion')->where($where);
        $query = $this->db->get();
        // print_r($where);
        // exit();
        return $query->row();
    }

    public function read_promotion_all()
    {
        $this->db->select('id_promotion,name_pro,option_pro,price_pro,date_pro,date_out,email')->from('promotion');
        $query = $this->db->get();
        return $query->result();
    }

    public function update_promotion($savedata)
    {
       
        $data = array(
            'id_promotion' => $savedata['id_promotion'],
            'name_pro' => $savedata['name_pro'],
            'option_pro' => $savedata['option_pro'],
            'price_pro' => $savedata['price_pro'],
            'date_pro' => $savedata['date_pro'],
            'date_out' => $savedata['date_out']

        );
        $this->db->where('id_promotion', $savedata['id_promotion']);
        $this->db->update('promotion', $data);
    }
    public function delete_promotion($id_promotion) {
        $this->db->where('id_promotion', $id_promotion);
        $this->db->delete('promotion');
        return $this->db->affected_rows();
    } 
    
}
