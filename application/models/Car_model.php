<?php

defined('BASEPATH') OR exit('NO direct script acces allowed');
class Car_model extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function create_car($savedata) {
        $data = array(
            'userid' => $savedata['userid'],
            'numberId_car' => $savedata['numberId_car'],
            'name_car' => $savedata['name_car'],
            'type_car' => $savedata['type_car'],
             
        );
        $this->db->insert('car', $data);
        return $this->db->insert_id();
    }
    public function read_car_by_id($userid) {
        $where = array(
            'userid' => $userid
        );
        $this->db->select('numberId_car,name_car,type_car')->from('car')->where($where);
        $query = $this->db->get();
        return $query->result();
        
    } 
    public function read_car() {
        $this->db->select('id_typecar,type_car')->from('typecar');
        $query = $this->db->get();
        return $query->result();
        
    } 
    public function read_carid($numberId_car) {
        $where = array(
            'numberId_car' => $numberId_car
        );
        $this->db->select('*')->from('car')->where($where);
        $query = $this->db->get();
        //file_put_contents('log.txt', "query : " . print_r($query,true) . PHP_EOL, FILE_APPEND);
        return $query->row();
        
    } 
}
