<?php

defined('BASEPATH') or exit('NO direct script acces allowed');

class Booking_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function read_namestore($email)
    {
        $where = array(
            'email' => $email,
        );
        $this->db->select('name_carcarestore,url_store');
        $this->db->from('carcarestore');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_by_id($email)
    {
        $where = array(
            'name_carcarestore' => $email,
        );
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_by_id1($queue)
    {
        $where = array(
            'queue' => $queue,
        );
        $this->db->select('queue,name,car_number,name_carcarestore,option,price,total,date,time,id_receipt,userid')->from('booking')->where($where);
        $query = $this->db->get();
        return $query->row();
    }
    public function message_curl($queue) {
        $this->db->set('status', 0);
        $this->db->where('queue', $queue);
        $this->db->update('booking');
        return $this->db->affected_rows();
    }

    
    ///ลูกค้า///
    public function date($email){
        $sql ="SELECT date.id_date,date.date_date FROM date
        LEFT JOIN booking ON date.id_date = booking.id_date
        WHERE date.date_date NOT IN (SELECT date.date_date FROM booking
        INNER JOIN date ON booking.id_date = date.id_date
        where booking.name_carcarestore ='$email' and booking.date = CURDATE())
        ORDER BY date.id_date ASC";

        $query = $this->db->query($sql);
        return $query->result();
    }
    public function typeoption($type_car,$email){
        $sql ="SELECT * FROM service WHERE `email` ='$email' and `option` LIKE '%$type_car%'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function read_booking_all()
    {
        $this->db->select('name_carcarestore,email')->from('carcarestore');
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_by_id3($userid)
    {
        $where = array(
            'userid' => $userid,
        );
        $this->db->select('*');
        $this->db->from('car');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_all1()
    {
        $this->db->select('numberId_car,name_car,type_car')->from('car');
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_namecar($numberId_car){
        $where = array(
            'numberId_car' => $numberId_car
        );
        $this->db->select('*')->from('car')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_option($email){
        $where = array(
            'email' => $email
        );
        $this->db->select('*')->from('service')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_booking_price($id_service){
        $where = array(
            'id_service' => $id_service
        );
        $this->db->select('*')->from('service')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_date($id_date){
        $where = array(
            'id_date' => $id_date
        );
        $this->db->select('*')->from('date')->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function update_booking($savedata)
    {
       
        $data = array(
            'date' => $savedata['date'],
  

        );
        $this->db->where('queue', $savedata['queue']);
        $this->db->update('booking', $data);
    }
    
    public function read_name($userid){
        $where = array(
            'userid' => $userid,
        );
        $this->db->select('*')->from('register')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_carnumber($car_number){
        $where = array(
            'numberId_car' => $car_number,
        );
        $this->db->select('name_car,type_car')->from('car')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function read_option($option){
        $where = array(
            'id_service' => $option,
        );
        $this->db->select('price,option')->from('service')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function create_booking($savedata)
    {
        $data = array(
            'userid' => $savedata['userid'],
            'name' => $savedata['name'],
            'last_name' => $savedata['last_name'],
            'name_carcarestore' => $savedata['name_carcarestore'],
            'name_car' => $savedata['name_car'],
            'car_number' => $savedata['car_number'],
            'type_car' => $savedata['type_car'],
            'name_promotion' => $savedata['name_promotion'],
            'option' => $savedata['option'],
            'price' => $savedata['price'],
            'total' => $savedata['total'],
            'time' => $savedata['time'],
            'id_date' => $savedata['id_date'],
            'date' => $savedata['date'],
            'id_receipt' => $savedata['id_receipt'],
            'img_receipt' => $savedata['img_receipt'],
            'status' => $savedata['status']

        );

        $this->db->insert('booking', $data);
        return $this->db->insert_id();
    }
    public function read_booking_by_id_payment($email)
    {
        $where = array(
            'email' => $email,
        );
        $this->db->select('bank_number,bank_bank,name_bank,img_bank')->from('carcarestore')->where($where);
        $query = $this->db->get();
        return $query->result();
    }
}
