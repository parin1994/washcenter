<?php
defined('BASEPATH') OR exit('NO direct script acces allowed');
class Carcarestore extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('carcarestore_model');
        $this->load->library('upload');
    }
    public function index(){
        $data['bank'] = $this->carcarestore_model->read_bank_all();
        $this->load->view('carcarestore',$data);
    }
    public function create(){
        $userid_carcarestore = $this->input->post('userid_carcarestore');
        $name_carcarestore = $this->input->post('name_carcarestore');
        $name = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $tel = $this->input->post('tel');
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $address = $this->input->post('address');
        $banknumber = $this->input->post('banknumber');
        $bank = $this->input->post('bank');
        $banknumber = $this->input->post('banknumber');
        $bankname = $this->input->post('bankname');

        $name_file = '';
            if (isset($_FILES['img']['name'])) {
                $config['upload_path'] = './assets/content';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '200';
                $this->load->library('upload', $config);
                $this->upload->do_upload('img');
                $source = './assets/content/' . $_FILES['img']['name'];

                $this->load->library('ftp');
                $ftp_config['hostname'] = '157.230.44.107';
                $ftp_config['username'] = 'appmoro_boteye';
                $ftp_config['password'] = 'qz8tXoa8r9';
                $ftp_config['debug']    = TRUE;

                //Connect to the remote server
                $this->ftp->connect($ftp_config);
                $this->ftp->upload($_FILES['img']['tmp_name'],"/public_html/assets/carcarestore/".$_FILES['img']['name'],"ascii", 0775);

                //Close FTP connection
                $this->ftp->close();
                $url = 'https://boteye.appmoro.com/assets/carcarestore/'. $_FILES['img']['name'];
                $name_file = './assets/carcarestore/' . $_FILES['img']['name'];
            }

            $qrcode = '';
            if (isset($_FILES['img']['name'])) {
                $config['upload_path'] = './assets/bank';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '200';
                $this->load->library('upload', $config);
                $this->upload->do_upload('img_bank');
                $source = './assets/content/' . $_FILES['img_bank']['name'];

                $this->load->library('ftp');
                $ftp_config['hostname'] = '157.230.44.107';
                $ftp_config['username'] = 'appmoro_boteye';
                $ftp_config['password'] = 'qz8tXoa8r9';
                $ftp_config['debug']    = TRUE;

                //Connect to the remote server
                $this->ftp->connect($ftp_config);
                $this->ftp->upload($_FILES['img_bank']['tmp_name'],"/public_html/assets/bank/".$_FILES['img_bank']['name'],"ascii", 0775);

                //Close FTP connection
                $this->ftp->close();
                $qrcode = './assets/bank/' . $_FILES['img_bank']['name'];
            }
        
    $savedata = array(
            'userid_carcarestore' => $userid_carcarestore,
            'name_carcarestore' => $name_carcarestore,
            'img' => $name_file,
            'name' => $name,
            'lastname' => $lastname,
            'tel' => $tel,
            'email' => $email,
            'pass' => $pass,
            'lat' => $lat,
            'lng' => $lng,
            'address' => $address,
            'date' => date("Y/m/d H:i:s"),
            'url_store' => $url,
            'bank_number' => $banknumber,
            'bank_bank' => $bank,
            'name_bank' => $bankname,
            'img_bank' => $qrcode,

    );
    $result = $this->carcarestore_model->create_carcarestore($savedata);
    redirect('carcareoffice/login/index');
    }
}
