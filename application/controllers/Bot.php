<?php

// กรณีต้องการตรวจสอบการแจ้ง error ให้เปิด 3 บรรทัดล่างนี้ให้ทำงาน กรณีไม่ ให้ comment ปิดไป
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// include composer autoload
require_once 'vendor/autoload.php';

// การตั้งเกี่ยวกับ bot
require_once 'bot_settings.php';

// กรณีมีการเชื่อมต่อกับฐานข้อมูล
//require_once("dbconnect.php");
///////////// ส่วนของการเรียกใช้งาน class ผ่าน namespace
use LINE\LINEBot;
use LINE\LINEBot\HTTPClient;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;
use LINE\LINEBot\Event;
use LINE\LINEBot\Event\BaseEvent;
use LINE\LINEBot\Event\MessageEvent;
use LINE\LINEBot\Event\AccountLinkEvent;
use LINE\LINEBot\Event\MemberJoinEvent;
use LINE\LINEBot\MessageBuilder;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use LINE\LINEBot\MessageBuilder\StickerMessageBuilder;
use LINE\LINEBot\MessageBuilder\ImageMessageBuilder;
use LINE\LINEBot\MessageBuilder\LocationMessageBuilder;
use LINE\LINEBot\MessageBuilder\AudioMessageBuilder;
use LINE\LINEBot\MessageBuilder\VideoMessageBuilder;
use LINE\LINEBot\ImagemapActionBuilder;
use LINE\LINEBot\ImagemapActionBuilder\AreaBuilder;
use LINE\LINEBot\ImagemapActionBuilder\ImagemapMessageActionBuilder;
use LINE\LINEBot\ImagemapActionBuilder\ImagemapUriActionBuilder;
use LINE\LINEBot\MessageBuilder\Imagemap\BaseSizeBuilder;
use LINE\LINEBot\MessageBuilder\ImagemapMessageBuilder;
use LINE\LINEBot\MessageBuilder\MultiMessageBuilder;
use LINE\LINEBot\TemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\DatetimePickerTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\PostbackTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateMessageBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ButtonTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ConfirmTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ImageCarouselTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ImageCarouselColumnTemplateBuilder;
use LINE\LINEBot\Constant\Flex\ComponentIconSize;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectRatio;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectMode;
use LINE\LINEBot\Constant\Flex\ComponentFontSize;
use LINE\LINEBot\Constant\Flex\ComponentFontWeight;
use LINE\LINEBot\Constant\Flex\ComponentMargin;
use LINE\LINEBot\Constant\Flex\ComponentSpacing;
use LINE\LINEBot\Constant\Flex\ComponentButtonStyle;
use LINE\LINEBot\Constant\Flex\ComponentButtonHeight;
use LINE\LINEBot\Constant\Flex\ComponentSpaceSize;
use LINE\LINEBot\Constant\Flex\ComponentGravity;
use LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use LINE\LINEBot\MessageBuilder\Flex\BubbleStylesBuilder;
use LINE\LINEBot\MessageBuilder\Flex\BlockStyleBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\CarouselContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\BoxComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ButtonComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\IconComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SpacerComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\FillerComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SeparatorComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;
use LINE\LINEBot\Event\MessageEvent\LocationMessage;

class Bot extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Car_model');
        $this->load->model('Carcare_model');
        $this->load->model('Register_model');
        $this->load->model('Price_model');
        $this->load->model('Carcarestore_model');
    }

    public function ToObject($Array)
    {

        // Clreate new stdClass object 
        $object = new stdClass();

        // Use loop to convert array into object 
        foreach ($Array as $key => $value) {
            if (is_array($value)) {
                $value = $this->ToObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }

    // public function getlocation($e,$lng,$lat,$address)
    // {
    //     $model = new Carcarestore_model;
    //     $result = $model->getlocation($lng,$lat,$address);

    //     file_put_contents('log.txt', "result : " . print_r($result, true) . PHP_EOL, FILE_APPEND);
    // }

    public function index()
    {
        // เชื่อมต่อกับ LINE Messaging API
        $httpClient = new CurlHTTPClient(LINE_MESSAGE_ACCESS_TOKEN);
        $bot = new LINEBot($httpClient, array('channelSecret' => LINE_MESSAGE_CHANNEL_SECRET));

        // คำสั่งรอรับการส่งค่ามาของ LINE Messaging API
        $content = file_get_contents('php://input');
        $hash = hash_hmac('sha256', $content, LINE_MESSAGE_CHANNEL_SECRET, true);
        $signature = base64_encode($hash);

        // แปลงค่าข้อมูลที่ได้รับจาก LINE เป็น array ของ Event Object
        $events = $bot->parseEventRequest($content, $signature);
        $eventObj = $events[0]; // Event Object ของ array แรก
        // ดึงค่าประเภทของ Event มาไว้ในตัวแปร มีทั้งหมด 7 event
        $eventType = $eventObj->getType();

        // แปลงข้อความรูปแบบ JSON  ให้อยู่ในโครงสร้างตัวแปร array
        // สร้างตัวแปร ไว้เก็บ sourceId ของแต่ละประเภท
        $userId = NULL;
        $sourceId = NULL;
        $sourceType = NULL;
        // สร้างตัวแปร replyToken และ replyData สำหรับกรณีใช้ตอบกลับข้อความ
        $result = NULL;
        $replyToken = NULL;
        $replyData = NULL;
        // สร้างตัวแปร ไว้เก็บค่าว่าเป้น Event ประเภทไหน
        $eventMessage = NULL;
        $eventPostback = NULL;
        $eventJoin = NULL;
        $eventLeave = NULL;
        $eventFollow = NULL;
        $eventUnfollow = NULL;
        $eventBeacon = NULL;
        $eventAccountLink = NULL;
        $eventMemberJoined = NULL;
        $eventMemberLeft = NULL;

        switch ($eventType) {
            case 'message':
                $eventMessage = true;
                break;
            case 'postback':
                $eventPostback = true;
                break;
            case 'join':
                $eventJoin = true;
                break;
            case 'leave':
                $eventLeave = true;
                break;
            case 'follow':
                $eventFollow = true;
                break;
            case 'unfollow':
                $eventUnfollow = true;
                break;
            case 'beacon':
                $eventBeacon = true;
                break;
            case 'accountLink':
                $eventAccountLink = true;
                break;
            case 'memberJoined':
                $eventMemberJoined = true;
                break;
            case 'memberLeft':
                $eventMemberLeft = true;
                break;
        }
        // สร้างตัวแปรเก็บค่า userId กรณีเป็น Event ที่เกิดขึ้นใน USER
        if ($eventObj->isUserEvent()) {
            $userId = $eventObj->getUserId();
            $sourceType = "USER";
        }

        $sourceId = $eventObj->getEventSourceId();
        if (is_null($eventLeave) && is_null($eventUnfollow) && is_null($eventMemberLeft)) {
            $replyToken = $eventObj->getReplyToken();
        }

        if (!is_null($eventPostback)) {
            $dataPostback = NULL;
            $paramPostback = NULL;
            // // แปลงข้อมูลจาก Postback Data เป็น array
            parse_str($eventObj->getPostbackData(), $dataPostback);
            // // ดึงค่า params กรณีมีค่า params
            $paramPostback = $eventObj->getPostbackParams();
            // file_put_contents('log.txt', "dataPostback : " . print_r($dataPostback, true) . PHP_EOL, FILE_APPEND);
            // file_put_contents('log.txt', "dataPostback : " . print_r($paramPostback, true) . PHP_EOL, FILE_APPEND);
            // // ทดสอบแสดงข้อความที่เกิดจาก Postaback Event
            //$textReplyMessage = "ข้อความจาก Postback Event Data = ";
            $resultObj = $this->ToObject($dataPostback);
            $textReplyMessage = json_encode($dataPostback);
            $placeName = $resultObj->address;
            $placeAddress = $resultObj->address;
            $latitude = $resultObj->lat;
            $longitude = $resultObj->lng;
            $replyData = new LocationMessageBuilder($placeName, $placeAddress, $latitude, $longitude);
            // $result = $this->Carcare_model->create_carcare($dataPostback);
            // $replyData = new TextMessageBuilder($textReplyMessage);


        }
        $events = json_decode($content, true);

        if (!is_null($events)) {
            if (!is_null($eventMessage)) {

                // สร้างตัวแปรเก็ยค่าประเภทของ Message จากทั้งหมด 7 ประเภท
                $typeMessage = $eventObj->getMessageType();
                //  text | image | sticker | location | audio | video | file  
                // เก็บค่า id ของข้อความ
                $idMessage = $eventObj->getMessageId();
                // ถ้าเป็นข้อความ
                if ($typeMessage == 'text') {
                    $userMessage = $eventObj->getText(); // เก็บค่าข้อความที่ผู้ใช้พิมพ์
                }
                // ถ้าเป็น image
                if ($typeMessage == 'image') {
                }
                // ถ้าเป็น audio
                if ($typeMessage == 'audio') {
                }
                // ถ้าเป็น video
                if ($typeMessage == 'video') {
                }
                if ($typeMessage == 'location') {
                    $locationTitle = $eventObj->getTitle();
                    $locationAddress = $eventObj->getAddress();
                    $locationLatitude = $eventObj->getLatitude();
                    $locationLongitude = $eventObj->getLongitude();
                    // $this->getlocation($events,$locationLongitude,$locationLatitude,$locationAddress);
                    $model = new Carcarestore_model;
                    $result = $model->getlocation($locationLongitude, $locationLatitude, $locationAddress);
                    $resultObj = $this->ToObject($result);
                    $columns = array();
                    //$img_url = "https://scontent.fbkk5-5.fna.fbcdn.net/v/t1.0-9/90273871_2852650564830014_225063025114087424_n.jpg?_nc_cat=100&_nc_sid=85a577&_nc_eui2=AeEVm8xmbUF7fANn-F3VU2bx4n4rNBIaWt3ifis0Ehpa3Q004nbkS52hmBl3Vx1wl1q0KLyJjOyB4bVePZu4oq76&_nc_oc=AQlOY5-NNtdH3Q6mZ5FISykHKh2W9pPM89Ms616a9UziS4xgBDXSXmU4rRU9YMrUeOU&_nc_ht=scontent.fbkk5-5.fna&oh=bf077b4217d60b88fab9376411318a68&oe=5ED8D238";
                    foreach ($resultObj as $pro) {

                        $actions = array(
                            new UriTemplateActionBuilder(
                                'จองโปรโมชัน', // ข้อความแสดงในปุ่ม
                                'https://liff.line.me/1653826205-Q3mkmJPp' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                            ),
                            new UriTemplateActionBuilder(
                                'โทร',
                                'tel:' . $pro->tel
                            ),
                            new PostbackTemplateActionBuilder(
                                'แผนที่', // ข้อความแสดงในปุ่ม
                                http_build_query(array(
                                    'address' => $pro->name,
                                    'lat' => $pro->lat,
                                    'lng' => $pro->lng,
                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                'แผนที่' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                            )
                        );
                        $img_url = $pro->url_store;
                        $column = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder($pro->name, "ระยะทาง : " . $pro->kilometers. " กม.", $img_url, $actions);
                        $columns[] = $column;
                    }
                    $carousel = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder($columns);
                    $replyData = new \LINE\LINEBot\MessageBuilder\TemplateMessageBuilder('Carousel', $carousel);
                }

                switch ($typeMessage) {
                    case 'text':
                        $userMessage = strtolower($userMessage);
                        switch ($userMessage) {

                                ////ดึงข้อมูล userid name รูป /////
                                //     case "t":
                                //         $actionBuilder = array(
                                //             new UriTemplateActionBuilder(
                                //                 'test', // ข้อความแสดงในปุ่ม
                                //                 'https://liff.line.me/1653826205-ge9k965J' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             )
                                //         );
                                //         $replyData = new TemplateMessageBuilder(
                                //             'Carousel',
                                //             new CarouselTemplateBuilder(
                                //                 array(
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'บริการล้างรถ',
                                //                         'เมืองขลุงคาร์แคร์',
                                //                         'https://image.makewebeasy.net/makeweb/0/u1Q8oIw6U/Home%2F003.png',
                                //                         $actionBuilder
                                //                     )
                                //                 )
                                //             )
                                //         );
                                // break;
                                //////////////////////////////////////////////

                            case "รายละเอียดร้านบริการทั้งหมด":
                                // กำหนด action 4 ปุ่ม 4 ประเภท
                                $actionBuilder = array(
                                    new UriTemplateActionBuilder(
                                        'ค้นหา', // ข้อความแสดงในปุ่ม
                                        'https://liff.line.me/1653826205-87bObmz1' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                    )
                                );
                                $replyData = new TemplateMessageBuilder(
                                    'Carousel',
                                    new CarouselTemplateBuilder(
                                        array(
                                            new CarouselColumnTemplateBuilder(
                                                'ร้านบริการทั้งหมด',
                                                'ค้นหาร้านบริการทั้งหมด',
                                                'https://boteye.appmoro.com/assets/wash/Parin.png',
                                                $actionBuilder
                                            )
                                        )
                                    )
                                );
                                break;


                            case "โปรโมชัน":
                                $textReplyMessage = new BubbleContainerBuilder(
                                    "ltr",  // กำหนด NULL หรือ "ltr" หรือ "rtl"
                                    NULL,
                                    NULL,
                                    new BoxComponentBuilder(
                                        "horizontal",
                                        array(
                                            new TextComponentBuilder("กดแชร์โลเคชั่นเพื่อค้นหา ร้านคาร์แคร์ใกล้คุณ", NULL, NULL, NULL, NULL, NULL, true)
                                        )
                                    ),
                                    new BoxComponentBuilder(
                                        "horizontal",
                                        array(
                                            new ButtonComponentBuilder(
                                                new UriTemplateActionBuilder("แชร์โลเคชั่น", "https://line.me/R/nv/location/"),
                                                NULL,
                                                NULL,
                                                NULL,
                                                "secondary"
                                            )
                                        )
                                    )
                                );
                                $replyData = new FlexMessageBuilder("Flex", $textReplyMessage);

                                break;

                            case "สมัครสมาชิก":
                                $response = $bot->getProfile($userId);
                                if ($response->isSucceeded()) {
                                    $userData = $response->getJSONDecodedBody(); // return array     
                                    $userId = $userData['userId'];
                                }
                                $result1 = $this->Register_model->read_register_by_id($userId);
                                $count1 = count($result1);
                                if ($count1 <= 0) {
                                    $actionBuilder = array(
                                        new UriTemplateActionBuilder(
                                            'สมัครสมาชิก', // ข้อความแสดงในปุ่ม
                                            'line://app/1653826205-6Oygy9LK' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        )
                                    );
                                    $replyData = new TemplateMessageBuilder(
                                        'Carousel',
                                        new CarouselTemplateBuilder(
                                            array(
                                                new CarouselColumnTemplateBuilder(
                                                    'สมัครสมาชิก',
                                                    'กรุณาลงทะเบียนครับ',
                                                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYWYd56i8EqfgVUrMipqArKxT9bALj-f9XSlUZRZCyS__ziJXD',
                                                    $actionBuilder
                                                )
                                            )
                                        )
                                    );
                                } else {
                                    $textReplyMessage = "คุณได้สมัครสมาชิกแล้ว";
                                    $replyData = new TextMessageBuilder($textReplyMessage);
                                }
                                break;



                            case "จองคิว":
                                $response = $bot->getProfile($userId);
                                if ($response->isSucceeded()) {
                                    $userData = $response->getJSONDecodedBody(); // return array     
                                    $userId = $userData['userId'];
                                }
                                $result1 = $this->Register_model->read_register_by_id($userId);
                                $count1 = count($result1);

                                if ($count1 <= 0) {
                                    //file_put_contents('log.txt', "count : " . print_r($result . TRUE) . PHP_EOL, FILE_APPEND);

                                    $actionBuilder = array(
                                        new UriTemplateActionBuilder(
                                            'สมัครสมาชิก', // ข้อความแสดงในปุ่ม
                                            'line://app/1653826205-6Oygy9LK' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        )
                                    );
                                    $replyData = new TemplateMessageBuilder(
                                        'Carousel',
                                        new CarouselTemplateBuilder(
                                            array(
                                                new CarouselColumnTemplateBuilder(
                                                    'สมัครสมาชิก',
                                                    'กรุณาลงทะเบียนครับ',
                                                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYWYd56i8EqfgVUrMipqArKxT9bALj-f9XSlUZRZCyS__ziJXD',
                                                    $actionBuilder
                                                )
                                            )
                                        )
                                    );
                                } else {
                                    $actionBuilder = array(
                                        new UriTemplateActionBuilder(
                                            'จองคิว', // ข้อความแสดงในปุ่ม
                                            'https://liff.line.me/1653826205-o52j260D'
                                        ),
                                        new UriTemplateActionBuilder(
                                            'แสดงการจอง', // ข้อความแสดงในปุ่ม
                                            'https://liff.line.me/1653826205-RvdGdXwZ'
                                        ),
                                        new UriTemplateActionBuilder(
                                            'ลงทะเบียนรถเพิ่ม', // ข้อความแสดงในปุ่ม
                                            'line://app/1653826205-nKwAwrXd'
                                        )
                                    );
                                    $replyData = new TemplateMessageBuilder(
                                        'Carousel',
                                        new CarouselTemplateBuilder(
                                            array(
                                                new CarouselColumnTemplateBuilder(
                                                    'บริการล้างรถ',
                                                    'WASH CENTER',
                                                    'https://scontent-fbkk5-7.us-fbcdn.net/v1/t.1-48/1426l78O9684I4108ZPH0J4S8_842023153_K1DlXQOI5DHP/dskvvc.qpjhg.xmwo/p/data/263/263314-5357536be8ad7.jpg',
                                                    $actionBuilder
                                                )
                                            )
                                        )
                                    );
                                }
                                break;



                            case "สำหรับร้านบริการ":
                                $actionBuilder = array(
                                    new UriTemplateActionBuilder(
                                        'เข้าสู่ระบบ', // ข้อความแสดงในปุ่ม
                                        'line://app/1653826205-ObpLpY5j' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                    ),
                                    new UriTemplateActionBuilder(
                                        'ลงทะเบียนร้านบริการ', // ข้อความแสดงในปุ่ม
                                        'line://app/1653826205-Xp5o5LBK' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                    )
                                );
                                $replyData = new TemplateMessageBuilder(
                                    'Carousel',
                                    new CarouselTemplateBuilder(
                                        array(
                                            new CarouselColumnTemplateBuilder(
                                                'เข้าสู่ระบบ',
                                                'กรุณาเข้าสู่ระบบ',
                                                'https://boteye.appmoro.com/assets/wash/Parin.png',
                                                $actionBuilder
                                            )
                                        )
                                    )
                                );

                                break;



                            case "ลงทะเบียนรถ":
                                // กำหนด action 4 ปุ่ม 4 ประเภท
                                $actionBuilder = array(
                                    new UriTemplateActionBuilder(
                                        'ลงทะเบียน', // ข้อความแสดงในปุ่ม
                                        'line://app/1653826205-nKwAwrXd' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                    )
                                );
                                $replyData = new TemplateMessageBuilder(
                                    'Carousel',
                                    new CarouselTemplateBuilder(
                                        array(
                                            new CarouselColumnTemplateBuilder(
                                                'บริการล้างรถ',
                                                'เมืองขลุงคาร์แคร์',
                                                'https://image.makewebeasy.net/makeweb/0/u1Q8oIw6U/Home%2F003.png',
                                                $actionBuilder
                                            )
                                        )
                                    )
                                );
                                break;

                            case "ติดต่อเรา":
                                // กำหนด action 4 ปุ่ม 4 ประเภท
                                $actionBuilder = array(
                                    //                                new UriTemplateActionBuilder(
                                    //                                        'Facebook', // ข้อความแสดงในปุ่ม
                                    //                                        'https://www.facebook.com/parin.zaa.39'
                                    //                                ),
                                    new UriTemplateActionBuilder(
                                        'โทร', // ข้อความแสดงในปุ่ม
                                        'tel://0951922340' // ข้อความแสดงในปุ่ม
                                    )
                                );
                                $replyData = new TemplateMessageBuilder(
                                    'Carousel',
                                    new CarouselTemplateBuilder(
                                        array(
                                            new CarouselColumnTemplateBuilder(
                                                'ติดต่อเรา',
                                                'เมืองขลุงคาร์แคร์',
                                                'https://image.freepik.com/free-photo/businesswoman-looking-important-contact-phone_1163-4256.jpg',
                                                $actionBuilder
                                            )
                                        )
                                    )
                                );
                                break;


                            case "ใบเสร็จ": // ส่วนทดสอบโต้ตอบข้อควมม flex
                                $response = $bot->getProfile($userId);
                                //                                    file_put_contents('log.txt',"userid : ".$userId . PHP_EOL, FILE_APPEND);
                                if ($response->isSucceeded()) {
                                    $userData = $response->getJSONDecodedBody(); // return array     
                                    $userId = $userData['userId'];
                                }
                                $result = $this->Carcare_model->read_carcare_by_id($userId);
                                //                            file_put_contents('log.txt', "result: " . print_r($result, TRUE) . PHP_EOL, FILE_APPEND);
                                $count = count($result);
                                if ($count > 0) {
                                    //$columns = array();

                                    foreach ($result as $value) {
                                        if ($value->receipt == 0) {
                                            //file_put_contents('log.txt', "value->receipt_carcare : " . print_r($value->receipt_carcare, TRUE) . PHP_EOL, FILE_APPEND);
                                            $textReplyMessage = new BubbleContainerBuilder(
                                                "ltr",
                                                NULL,
                                                NULL,
                                                new BoxComponentBuilder(
                                                    "vertical",
                                                    array(
                                                        new TextComponentBuilder("RECEIPT", NULL, NULL, "md", NULL, NULL, NULL, NULL, "bold", "#1DB446"),
                                                        new TextComponentBuilder("Carcare Service", NULL, NULL, "xl", NULL, NULL, NULL, NULL, "bold", "#000033"),
                                                        new TextComponentBuilder(" "),
                                                        new TextComponentBuilder("order" . " " . ": " . $value->queue_carcare),
                                                        new TextComponentBuilder("date" . " " . "  : " . $value->date),
                                                        new TextComponentBuilder(" "),
                                                        new TextComponentBuilder("ยังไม่ได้ชำระเงิน"),
                                                        new TextComponentBuilder(" "),
                                                        new TextComponentBuilder(" "),
                                                        new BoxComponentBuilder(
                                                            "vertical",
                                                            array(
                                                                new BoxComponentBuilder(
                                                                    "horizontal",
                                                                    array(
                                                                        new TextComponentBuilder("ITEM"),
                                                                        new TextComponentBuilder(" "),
                                                                        new TextComponentBuilder(" "),
                                                                        new TextComponentBuilder("       1"),
                                                                        new TextComponentBuilder("  ITEM"),
                                                                    )
                                                                ),
                                                                new BoxComponentBuilder(
                                                                    "horizontal",
                                                                    array(
                                                                        new TextComponentBuilder($value->option),
                                                                        new TextComponentBuilder($value->type),
                                                                        new TextComponentBuilder($value->price . " " . "  THB")
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        new TextComponentBuilder(" "),
                                                        new SeparatorComponentBuilder(),
                                                        new TextComponentBuilder(" "),
                                                        new BoxComponentBuilder(
                                                            "vertical",
                                                            array(
                                                                new BoxComponentBuilder(
                                                                    "horizontal",
                                                                    array(
                                                                        new TextComponentBuilder("TOTAL"),
                                                                        new TextComponentBuilder(" "),
                                                                        new TextComponentBuilder($value->price . " " . "  THB"),
                                                                    )
                                                                ),
                                                                new SeparatorComponentBuilder(),
                                                            )
                                                        ),
                                                    )
                                                )
                                            );
                                            //                                //$column = new \LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder($textReplyMessage);
                                            //                                    $columns[] = $textReplyMessage;
                                            $replyData = new FlexMessageBuilder("This is a Flex Message", $textReplyMessage);
                                        } else {
                                            $textReplyMessage = "คุณยังไม่ได้จองคิวรถ";
                                            $replyData = new TextMessageBuilder($textReplyMessage);
                                        }
                                    }
                                    //                                $carousel = new \LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder($columns);
                                    //                                $replyData = new \LINE\LINEBot\MessageBuilder\FlexMessageBuilder('Carousel', $columns);
                                } else {
                                    $textReplyMessage = "คุณยังไม่ได้จองคิวรถ";
                                    $replyData = new TextMessageBuilder($textReplyMessage);
                                }
                                break;
                            default:
                                $textReplyMessage = "สามารถเลือกดูเมนูต่างๆภายในร้านค้าได้ คลิกที่เมนูด้านล่างครับ";
                                $replyData = new TextMessageBuilder($textReplyMessage);
                                break;
                        }
                        break;

                        // default:
                        //     $textReplyMessage = json_encode($events);
                        //     $replyData = new TextMessageBuilder($textReplyMessage);
                        //     break;
                }
            }
            //l ส่วนของคำสั่งตอบกลับข้อความ
            $response = $bot->replyMessage($replyToken, $replyData);
            //        $err_msg = $response->getHTTPStatus() . " " . $response->getRawBody();
            //        file_put_contents('error_log.txt', print_r($err_msg, TRUE) . PHP_EOL, FILE_APPEND);
        }
    }
}




////////////////////////////////////////////////////////////////////////////
             // case "ข้อมูลรถ":
                            //     $response = $bot->getProfile($userId);
                            //     //                                    file_put_contents('log.txt',"userid : ".$userId . PHP_EOL, FILE_APPEND);
                            //     if ($response->isSucceeded()) {
                            //         $userData = $response->getJSONDecodedBody(); // return array     
                            //         $userId = $userData['userId'];
                            //     }

                            //     $result = $this->Car_model->read_car_by_id($userId);

                            //     //file_put_contents('log.txt', "result : " . print_r($result, true) . PHP_EOL, FILE_APPEND);
                            //     $count = count($result);
                            //     //file_put_contents('log.txt', "count : " . print_r($count, true) . PHP_EOL, FILE_APPEND);
                            //     if ($count > 0) {
                            //         $columns = array();
                            //         $img_url = "https://cdn.marketingoops.com/wp-content/uploads/2012/07/aud-h.jpg";
                            //         foreach ($result as $value) {
                            //             $price = $this->Price_model->read_price_by_id($value->type_car);
                            //             $actions = array(
                            //                 new PostbackTemplateActionBuilder(
                            //                     'ล้างสี+ดูดฝุ่น     ' . $price->price1, // ข้อความแสดงในปุ่ม
                            //                     http_build_query(array(
                            //                         'userid' => $userId,
                            //                         'name' => $value->name_car,
                            //                         'type' => $value->type_car,
                            //                         'carnumber' => $value->numberid_car,
                            //                         'option' => 'ล้างสี',
                            //                         'price' => $price->price1,
                            //                         'receipt' => 0
                            //                     )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                            //                     'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                            //                 ),
                            //                 new PostbackTemplateActionBuilder(
                            //                     'ล้างอัดฉีด     ' . $price->price2, // ข้อความแสดงในปุ่ม
                            //                     http_build_query(array(
                            //                         'userid' => $userId,
                            //                         'name' => $value->name_car,
                            //                         'type' => $value->type_car,
                            //                         'carnumber' => $value->numberid_car,
                            //                         'option' => 'อัดฉีด',
                            //                         'price' => $price->price2,
                            //                         'receipt' => 0
                            //                     )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                            //                     'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                            //                 ),
                            //                 new PostbackTemplateActionBuilder(
                            //                     'ล้างสี+ขัดเงา     ' . $price->price3, // ข้อความแสดงในปุ่ม
                            //                     http_build_query(array(
                            //                         'userid' => $userId,
                            //                         'name' => $value->name_car,
                            //                         'type' => $value->type_car,
                            //                         'carnumber' => $value->carnumberid_car,
                            //                         'option' => 'ล้างขัดเงา',
                            //                         'price' => $price->price3,
                            //                         'receipt' => 0
                            //                     )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                            //                     'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                            //                 ),
                            //             );
                            //             $column = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder($value->name_car, "ทะเบียนรถ : " . $value->carnumber_car, $img_url, $actions);
                            //             $columns[] = $column;
                            //         }
                            //         $carousel = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder($columns);
                            //         $replyData = new \LINE\LINEBot\MessageBuilder\TemplateMessageBuilder('Carousel', $carousel);
                            //     } else {
                            //         $actionBuilder = array(
                            //             new UriTemplateActionBuilder(
                            //                 'ลงทะเบียน', // ข้อความแสดงในปุ่ม
                            //                 'line://app/1653826205-nKwAwrXd'
                            //             )
                            //         );
                            //         $replyData = new TemplateMessageBuilder(
                            //             'Carousel',
                            //             new CarouselTemplateBuilder(
                            //                 array(
                            //                     new CarouselColumnTemplateBuilder(
                            //                         'ลงทะเบียนข้อมูลรถ',
                            //                         'ไม่มีข้อมูลรถของท่านกรุณาลงทะเบียน',
                            //                         'https://image.makewebeasy.net/makeweb/0/u1Q8oIw6U/Home%2F003.png',
                            //                         $actionBuilder
                            //                     )
                            //                 )
                            //             )
                            //         );
                            //     }
                            //     break;


            // case "แผนที่":
            //     $placeName = "คาร์แคร์ เซอร์วิส";
            //     $placeAddress = "เลขที่ 36/6 เมือง, ถนนสุขุมวิท ตำบล ขลุง อำเภอ ขลุง จันทบุรี 22110";
            //     $latitude = 12.461231;
            //     $longitude = 102.228958;
            //     $replyData = new LocationMessageBuilder($placeName, $placeAddress, $latitude, $longitude);
            //     break;

             // case "สำหรับร้านค้า":
                                //     // กำหนด action 4 ปุ่ม 4 ประเภท
                                //     $actionBuilder = array(
                                //         new UriTemplateActionBuilder(
                                //             'เข้าสู่ระบบร้านค้า', // ข้อความแสดงในปุ่ม
                                //             'line://app/1653826205-ObpLpY5j' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //         )
                                //     );
                                //     $replyData = new TemplateMessageBuilder(
                                //         'Carousel',
                                //         new CarouselTemplateBuilder(
                                //             array(
                                //                 new CarouselColumnTemplateBuilder(
                                //                     'ลงทะเบียน',
                                //                     'กรุณาลงทะเบียนร้านค้า',
                                //                     'https://www.roojai.com/wp-content/uploads/2017/07/chic-cafe-garage-01.jpg',
                                //                     $actionBuilder
                                //                 )
                                //             )
                                //         )
                                //     );
                                //     break;            

            // case "โปรโมชัน":
                                //     $response = $bot->getProfile($userId);
                                //     //                                    file_put_contents('log.txt',"userid : ".$userId . PHP_EOL, FILE_APPEND);
                                //     if ($response->isSucceeded()) {
                                //         $userData = $response->getJSONDecodedBody(); // return array     
                                //         $userId = $userData['userId'];
                                //     }

                                //     $result1 = $this->Register_model->read_register_by_id($userId);
                                //     $count1 = count($result1);

                                //     if ($count1 <= 0) {
                                //         //file_put_contents('log.txt', "count : " . print_r($result . TRUE) . PHP_EOL, FILE_APPEND);

                                //         $actionBuilder = array(
                                //             new UriTemplateActionBuilder(
                                //                 'สมัครสมาชิก', // ข้อความแสดงในปุ่ม
                                //                 'line://app/1601635398-Z6DWBOy1' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             )
                                //         );
                                //         $replyData = new TemplateMessageBuilder(
                                //             'Carousel',
                                //             new CarouselTemplateBuilder(
                                //                 array(
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'สมัครสมาชิก',
                                //                         'กรุณาลงทะเบียนครับ',
                                //                         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYWYd56i8EqfgVUrMipqArKxT9bALj-f9XSlUZRZCyS__ziJXD',
                                //                         $actionBuilder
                                //                     )
                                //                 )
                                //             )
                                //         );
                                //     } else {

                                //         // กำหนด action 4 ปุ่ม 4 ประเภท
                                //         $actionBuilder = array(
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ดูดฝุ่น     150 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถ6ล้อ',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างสี',
                                //                     'price' => 150,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างอัดฉีด     250 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถ6ล้อ',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'อัดฉีด',
                                //                     'price' => 250,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ขัดเงา     450 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถ6ล้อ',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างขัดเงา',
                                //                     'price' => 450,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //         );
                                //         //                              file_put_contents('log.txt',"actionBuilder : ".print_r($actionBuilder.TRUE) . PHP_EOL, FILE_APPEND);
                                //         $actionBuilder1 = array(
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ดูดฝุ่น     110 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถตู้',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างสี',
                                //                     'price' => 110,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างอัดฉีด     150 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถตู้',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'อัดฉีด',
                                //                     'price' => 150,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ขัดเงา     275 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถตู้',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างขัดเงา',
                                //                     'price' => 275,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //         );

                                //         $actionBuilder2 = array(
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ดูดฝุ่น     90 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถกระบะ',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างสี',
                                //                     'price' => 90,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างอัดฉีด     125 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถกระบะ',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'อัดฉีด',
                                //                     'price' => 125,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ขัดเงา     240 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถกระบะ',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างขัดเงา',
                                //                     'price' => 240,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //         );

                                //         $actionBuilder3 = array(
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ดูดฝุ่น     75 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถเก๋ง',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างสี',
                                //                     'price' => 75,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างอัดฉีด     110 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถเก๋ง',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'อัดฉีด',
                                //                     'price' => 110,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ขัดเงา     190 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถเก๋ง',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างขัดเงา',
                                //                     'price' => 190,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //         );

                                //         $actionBuilder4 = array(
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี     40 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถมอไซต์',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างสี',
                                //                     'price' => 40,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างอัดฉีด     70 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถมอไซต์',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'อัดฉีด',
                                //                     'price' => 70,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //             new PostbackTemplateActionBuilder(
                                //                 'ล้างสี+ขัดเงา     100 B', // ข้อความแสดงในปุ่ม
                                //                 http_build_query(array(
                                //                     'userid' => $userId,
                                //                     'name' => 'pro',
                                //                     'type' => 'รถมอไซต์',
                                //                     'carnumber' => 'pro',
                                //                     'option' => 'ล้างขัดเงา',
                                //                     'price' => 100,
                                //                     'receipt' => 0
                                //                 )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                //                 'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                //             ),
                                //         );
                                //         $replyData = new TemplateMessageBuilder(
                                //             'Carousel',
                                //             new CarouselTemplateBuilder(
                                //                 array(
                                //                     //                                new CarouselColumnTemplateBuilder(
                                //                     //                                        'โปรโมชั่น เดือน สิงหาคม', 'ล้างรถ 10 ครั้ง ฟรี 1 ครั้ง', 'https://image.freepik.com/free-vector/modern-sale-banner-colorful-comic-style_1361-1314.jpg', $actionBuilder
                                //                     //                                ),
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'โปรโมชั่น เดือน สิงหาคม ลด 50 %',
                                //                         'รถ 6 ล้อ ',
                                //                         'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg',
                                //                         $actionBuilder
                                //                     ),
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'โปรโมชั่น เดือน สิงหาคม ลด 50 %',
                                //                         'รถ ตู้ ',
                                //                         'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg',
                                //                         $actionBuilder1
                                //                     ),
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'โปรโมชั่น เดือน สิงหาคม ลด 50 %',
                                //                         'รถ กะบะ ',
                                //                         'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg',
                                //                         $actionBuilder2
                                //                     ),
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'โปรโมชั่น เดือน สิงหาคม ลด 50 %',
                                //                         'รถ เก๋ง ',
                                //                         'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg',
                                //                         $actionBuilder3
                                //                     ),
                                //                     new CarouselColumnTemplateBuilder(
                                //                         'โปรโมชั่น เดือน สิงหาคม ลด 50 %',
                                //                         'รถ มอเตอร์ไซต์ ',
                                //                         'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg',
                                //                         $actionBuilder4
                                //                     ),
                                //                 )
                                //             )
                                //         );
                                //         //file_put_contents('log.txt', "replyData : " . print_r($replyData , TRUE) . PHP_EOL, FILE_APPEND);
                                //     }
                                //     break;
