<?php
defined('BASEPATH') or exit('NO direct script acces allowed');

class Storeall extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->model('storeall_model');
       
    }
    public function index()
    {
        $data['title'] = 'ร้านทั้งหมด';
        $data['header_cus'] = $this->load->view('carcareoffice/include/header_cus', '', TRUE);
        $data['read'] = $this->storeall_model->read_storeall_all();
        $this->load->view('storeall', $data);
    }
    public function test()
    {
        $data['title'] = 'ร้านทั้งหมด';
        $data['header_cus'] = $this->load->view('carcareoffice/include/header_cus', '', TRUE);
        $data['read'] = $this->storeall_model->read_storeall_all();
        $this->load->view('storeall', $data);
    }
   public function search(){
    $data['title'] = 'ร้านทั้งหมด';
    $data['header_cus'] = $this->load->view('carcareoffice/include/header_cus', '', TRUE);
        $key = $this->input->post('title');
        $data['read'] = $this->storeall_model->search($key);
        $this->load->view('storeall', $data);
     }
   }

