<?php

defined('BASEPATH') OR exit('NO direct script acces allowed');

class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('register_model');
    }

    public function index() {
        $this->load->view('register');
    }

    public function create() {
        $userid = $this->input->post('userid');
        $name_register = $this->input->post('name_register');
        $lastname_register = $this->input->post('lastname_register');
        $email_register = $this->input->post('email_register');
        $phone_register = $this->input->post('phone_register');
        $address_register = $this->input->post('address_register');

        $savedata = array(
            'userid' => $userid,
            'name_register' => $name_register,
            'lastname_register' => $lastname_register,
            'email_register' => $email_register,
            'phone_register' => $phone_register,
            'address_register' => $address_register,
            'date_register' => date("Y/m/d H:i:s")
        );
//        echo '<pre>';
//        print_r($savedata);
//        echo '</pre>';
        $result = $this->register_model->create_register($savedata);
        redirect('car');
    }

}
