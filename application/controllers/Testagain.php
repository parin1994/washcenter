<?php

// กรณีต้องการตรวจสอบการแจ้ง error ให้เปิด 3 บรรทัดล่างนี้ให้ทำงาน กรณีไม่ ให้ comment ปิดไป
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// include composer autoload
require_once 'vendor/autoload.php';

// การตั้งเกี่ยวกับ bot
require_once 'bot_settings.php';

// กรณีมีการเชื่อมต่อกับฐานข้อมูล
//require_once("dbconnect.php");
///////////// ส่วนของการเรียกใช้งาน class ผ่าน namespace
use LINE\LINEBot;
use LINE\LINEBot\HTTPClient;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;
use LINE\LINEBot\Event;
use LINE\LINEBot\Event\BaseEvent;
use LINE\LINEBot\Event\MessageEvent;
use LINE\LINEBot\Event\AccountLinkEvent;
use LINE\LINEBot\Event\MemberJoinEvent;
use LINE\LINEBot\MessageBuilder;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use LINE\LINEBot\MessageBuilder\StickerMessageBuilder;
use LINE\LINEBot\MessageBuilder\ImageMessageBuilder;
use LINE\LINEBot\MessageBuilder\LocationMessageBuilder;
use LINE\LINEBot\MessageBuilder\AudioMessageBuilder;
use LINE\LINEBot\MessageBuilder\VideoMessageBuilder;
use LINE\LINEBot\ImagemapActionBuilder;
use LINE\LINEBot\ImagemapActionBuilder\AreaBuilder;
use LINE\LINEBot\ImagemapActionBuilder\ImagemapMessageActionBuilder;
use LINE\LINEBot\ImagemapActionBuilder\ImagemapUriActionBuilder;
use LINE\LINEBot\MessageBuilder\Imagemap\BaseSizeBuilder;
use LINE\LINEBot\MessageBuilder\ImagemapMessageBuilder;
use LINE\LINEBot\MessageBuilder\MultiMessageBuilder;
use LINE\LINEBot\TemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\DatetimePickerTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\PostbackTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateMessageBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ButtonTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ConfirmTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ImageCarouselTemplateBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ImageCarouselColumnTemplateBuilder;
use LINE\LINEBot\Constant\Flex\ComponentIconSize;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectRatio;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectMode;
use LINE\LINEBot\Constant\Flex\ComponentFontSize;
use LINE\LINEBot\Constant\Flex\ComponentFontWeight;
use LINE\LINEBot\Constant\Flex\ComponentMargin;
use LINE\LINEBot\Constant\Flex\ComponentSpacing;
use LINE\LINEBot\Constant\Flex\ComponentButtonStyle;
use LINE\LINEBot\Constant\Flex\ComponentButtonHeight;
use LINE\LINEBot\Constant\Flex\ComponentSpaceSize;
use LINE\LINEBot\Constant\Flex\ComponentGravity;
use LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use LINE\LINEBot\MessageBuilder\Flex\BubbleStylesBuilder;
use LINE\LINEBot\MessageBuilder\Flex\BlockStyleBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\CarouselContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\BoxComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ButtonComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\IconComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SpacerComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\FillerComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SeparatorComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;

class Testagain extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Car_model');
        $this->load->model('Carcare_model');
        $this->load->model('Register_model');
    }

    public function index() {
// เชื่อมต่อกับ LINE Messaging API
        $httpClient = new CurlHTTPClient(LINE_MESSAGE_ACCESS_TOKEN);
        $bot = new LINEBot($httpClient, array('channelSecret' => LINE_MESSAGE_CHANNEL_SECRET));

// คำสั่งรอรับการส่งค่ามาของ LINE Messaging API
        $content = file_get_contents('php://input');
        $hash = hash_hmac('sha256', $content, LINE_MESSAGE_CHANNEL_SECRET, true);
        $signature = base64_encode($hash);

// แปลงค่าข้อมูลที่ได้รับจาก LINE เป็น array ของ Event Object
        $events = $bot->parseEventRequest($content, $signature);
        $eventObj = $events[0]; // Event Object ของ array แรก
// ดึงค่าประเภทของ Event มาไว้ในตัวแปร มีทั้งหมด 7 event
        $eventType = $eventObj->getType();

// แปลงข้อความรูปแบบ JSON  ให้อยู่ในโครงสร้างตัวแปร array
        // สร้างตัวแปร ไว้เก็บ sourceId ของแต่ละประเภท
        $userId = NULL;
        $sourceId = NULL;
        $sourceType = NULL;
        // สร้างตัวแปร replyToken และ replyData สำหรับกรณีใช้ตอบกลับข้อความ
        $result = NULL;
        $replyToken = NULL;
        $replyData = NULL;
        // สร้างตัวแปร ไว้เก็บค่าว่าเป้น Event ประเภทไหน
        $eventMessage = NULL;
        $eventPostback = NULL;
        $eventJoin = NULL;
        $eventLeave = NULL;
        $eventFollow = NULL;
        $eventUnfollow = NULL;
        $eventBeacon = NULL;
        $eventAccountLink = NULL;
        $eventMemberJoined = NULL;
        $eventMemberLeft = NULL;

        switch ($eventType) {
            case 'message': $eventMessage = true;
                break;
            case 'postback': $eventPostback = true;
                break;
            case 'join': $eventJoin = true;
                break;
            case 'leave': $eventLeave = true;
                break;
            case 'follow': $eventFollow = true;
                break;
            case 'unfollow': $eventUnfollow = true;
                break;
            case 'beacon': $eventBeacon = true;
                break;
            case 'accountLink': $eventAccountLink = true;
                break;
            case 'memberJoined': $eventMemberJoined = true;
                break;
            case 'memberLeft': $eventMemberLeft = true;
                break;
        }
// สร้างตัวแปรเก็บค่า userId กรณีเป็น Event ที่เกิดขึ้นใน USER
        if ($eventObj->isUserEvent()) {
            $userId = $eventObj->getUserId();
            $sourceType = "USER";
        }

        $sourceId = $eventObj->getEventSourceId();
        if (is_null($eventLeave) && is_null($eventUnfollow) && is_null($eventMemberLeft)) {
            $replyToken = $eventObj->getReplyToken();
        }
        if (!is_null($eventPostback)) {
            $dataPostback = NULL;
            $paramPostback = NULL;
            // แปลงข้อมูลจาก Postback Data เป็น array
            parse_str($eventObj->getPostbackData(), $dataPostback);
            // ดึงค่า params กรณีมีค่า params
            $paramPostback = $eventObj->getPostbackParams();
            // ทดสอบแสดงข้อความที่เกิดจาก Postaback Event
            //$textReplyMessage = "ข้อความจาก Postback Event Data = ";
            $textReplyMessage .= json_encode($dataPostback);
            //$textReplyMessage .= json_encode($paramPostback);
            $result = $this->Carcare_model->create_carcare($dataPostback);
            file_put_contents('log.txt', "dataPostback : " . print_r($dataPostback, true) . PHP_EOL, FILE_APPEND);
            file_put_contents('log.txt', "textReplyMessage : " . print_r($textReplyMessage, true) . PHP_EOL, FILE_APPEND);
//                $replyData = new TextMessageBuilder($textReplyMessage);
        }
        $events = json_decode($content, true);
        if (!is_null($events)) {
            // ถ้ามีค่า สร้างตัวแปรเก็บ replyToken ไว้ใช้งาน
            $replyToken = $events['events'][0]['replyToken'];
            $typeMessage = $events['events'][0]['message']['type'];
            $userMessage = $events['events'][0]['message']['text'];
            $userMessage = strtolower($userMessage);
            switch ($typeMessage) {
                case 'text':
                    switch ($userMessage) {

                        case "จองคิว":
                            $response = $bot->getProfile($userId);
//                                    file_put_contents('log.txt',"userid : ".$userId . PHP_EOL, FILE_APPEND);
                            if ($response->isSucceeded()) {
                                $userData = $response->getJSONDecodedBody(); // return array     
                                $userId = $userData['userId'];
                            }

                            $result1 = $this->Register_model->read_register_by_id($userId);
                            $count1 = count($result1);

                            if ($count1 <= 0) {
                                file_put_contents('log.txt', "count : " . print_r($result . TRUE) . PHP_EOL, FILE_APPEND);

                                $actionBuilder = array(
                                    new UriTemplateActionBuilder(
                                            'สมัครสมาชิก', // ข้อความแสดงในปุ่ม
                                            'line://app/1601635398-Z6DWBOy1' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                    )
                                );
                                $replyData = new TemplateMessageBuilder('Carousel', new CarouselTemplateBuilder(
                                        array(
                                    new CarouselColumnTemplateBuilder(
                                            'สมัครสมาชิก', 'กรุณาลงทะเบียนด้วยครับ', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYWYd56i8EqfgVUrMipqArKxT9bALj-f9XSlUZRZCyS__ziJXD', $actionBuilder
                                    )
                                        )
                                        )
                                );
                            } else {
                            $actionBuilder = array(
                                new MessageTemplateActionBuilder(
                                        'จองคิว', // ข้อความแสดงในปุ่ม
                                        'ข้อมูลรถ'
                                ),
                                new MessageTemplateActionBuilder(
                                        'แสดงคิว', // ข้อความแสดงในปุ่ม
                                        'แสดงคิว'
                                )
                            );
                            $replyData = new TemplateMessageBuilder('Carousel', new CarouselTemplateBuilder(
                                    array(
                                new CarouselColumnTemplateBuilder(
                                        'บริการล้างรถ', 'เมืองขลุงคาร์แคร์', 'https://scontent-fbkk5-7.us-fbcdn.net/v1/t.1-48/1426l78O9684I4108ZPH0J4S8_842023153_K1DlXQOI5DHP/dskvvc.qpjhg.xmwo/p/data/263/263314-5357536be8ad7.jpg', $actionBuilder
                                )
                                    )
                                    )
                            );
                            }
                            break;

                        case "ข้อมูลรถ":
                            $response = $bot->getProfile($userId);
                            if ($response->isSucceeded()) {
                                $userData = $response->getJSONDecodedBody(); // return array     
                                $userId = $userData['userId'];
                            }

                            $result = $this->Car_model->read_car_by_id($userId);
                            file_put_contents('log.txt', "result : " . print_r($result, true) . PHP_EOL, FILE_APPEND);
                            $count = count($result);
                            file_put_contents('log.txt', "count : " . print_r($count, true) . PHP_EOL, FILE_APPEND);
                            
                            if ($count > 0) {
                                $columns = array();
                                $img_url = "https://cdn.marketingoops.com/wp-content/uploads/2012/07/aud-h.jpg";
                                foreach ($result as $value) {
                                    $actions = array(
                                        new PostbackTemplateActionBuilder(
                                                'ล้างอัดฉีด     180 B', // ข้อความแสดงในปุ่ม
                                                http_build_query(array(
                                                    'userid' => $userId,
                                                    'name_carcare' => $value->name_car,
                                                    'type_carcare' => $value->type_car,
                                                    'carnumber_carcare' => $value->carnumber_car,
                                                    'option_carcare' => 'ล้างอัดฉีด',
                                                    'price_carcare' => 180,
                                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                                'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        ),
                                        new PostbackTemplateActionBuilder(
                                                'ล้างสี+ดูดฝุ่น     350 B', // ข้อความแสดงในปุ่ม
                                                http_build_query(array(
                                                    'userid' => $userId,
                                                    'name_carcare' => $value->name_car,
                                                    'type_carcare' => $value->type_car,
                                                    'carnumber_carcare' => $value->carnumber_car,
                                                    'option_carcare' => 'ล้างสี+ดูดฝุ่น',
                                                    'price_carcare' => 350,
                                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                                'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        ),
                                        new PostbackTemplateActionBuilder(
                                                'ล้างสี+ขัดเงา     500 B', // ข้อความแสดงในปุ่ม
                                                http_build_query(array(
                                                    'userid' => $userId,
                                                    'name_carcare' => $value->name_car,
                                                    'type_carcare' => $value->type_car,
                                                    'carnumber_carcare' => $value->carnumber_car,
                                                    'option_carcare' => 'ล้างสี+ขัดเงา',
                                                    'price_carcare' => 500,
                                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                                'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        ),
                                    );
                                    //file_put_contents('log.txt', "actions : " . print_r($actions, true) . PHP_EOL, FILE_APPEND);
                                    $column = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder($value->name_car, "ทะเบียนรถ : " . $value->carnumber_car, $img_url, $actions);
                                    $columns[] = $column;
                                }
                                $carousel = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder($columns);
                                $replyData = new \LINE\LINEBot\MessageBuilder\TemplateMessageBuilder('Carousel', $carousel);
                                //file_put_contents('log.txt', "replyData : " . print_r($replyData, true) . PHP_EOL, FILE_APPEND);
                            } else {
                                $actionBuilder = array(
                                    new UriTemplateActionBuilder(
                                            'ลงทะเบียน', // ข้อความแสดงในปุ่ม
                                            'line://app/1601635398-10npMRv8'
                                    )
                                );
                                $replyData = new TemplateMessageBuilder('Carousel', new CarouselTemplateBuilder(
                                        array(
                                    new CarouselColumnTemplateBuilder(
                                            'ลงทะเบียนข้อมูลรถ', 'ไม่มีข้อมูลรถของท่านกรุณาลงทะเบียน', 'https://image.makewebeasy.net/makeweb/0/u1Q8oIw6U/Home%2F003.png', $actionBuilder
                                    )
                                        )
                                        )
                                );
                            }
                            break;

                        case "ลงทะเบียนรถ":
                            // กำหนด action 4 ปุ่ม 4 ประเภท
                            $actionBuilder = array(
                                new UriTemplateActionBuilder(
                                        'ลงทะเบียน', // ข้อความแสดงในปุ่ม
                                        'line://app/1601635398-10npMRv8' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                )
                            );
                            $replyData = new TemplateMessageBuilder('Carousel', new CarouselTemplateBuilder(
                                    array(
                                new CarouselColumnTemplateBuilder(
                                        'บริการล้างรถ', 'เมืองขลุงคาร์แคร์', 'https://image.makewebeasy.net/makeweb/0/u1Q8oIw6U/Home%2F003.png', $actionBuilder
                                )
                                    )
                                    )
                            );
                            break;

                        case "โปรโมชั่น":
                            $response = $bot->getProfile($userId);
//                                    file_put_contents('log.txt',"userid : ".$userId . PHP_EOL, FILE_APPEND);
                            if ($response->isSucceeded()) {
                                $userData = $response->getJSONDecodedBody(); // return array     
                                $userId = $userData['userId'];
                            }
                            
                            // กำหนด action 4 ปุ่ม 4 ประเภท
                            $actionBuilder = array(
                                new PostbackTemplateActionBuilder(
                                                'ล้างอัดฉีด     90 B', // ข้อความแสดงในปุ่ม
                                                http_build_query(array(
                                                    'userid' => $userId,
                                                    'name_carcare' => 'promotion',
                                                    'type_carcare' => 'รถ 6 ล้อ',
                                                    'carnumber_carcare' => 'promotion',
                                                    'option_carcare' => 'ล้างสี+ขัดเงา',
                                                    'price_carcare' => 90
                                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                                'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        ),
                                new PostbackTemplateActionBuilder(
                                                'ล้างสี+ดูดฝุ่น     175 B', // ข้อความแสดงในปุ่ม
                                                http_build_query(array(
                                                    'userid' => $userId,
                                                    'name_carcare' => 'promotion',
                                                    'type_carcare' => 'รถ 6 ล้อ',
                                                    'carnumber_carcare' => 'promotion',
                                                    'option_carcare' => 'ล้างสี+ขัดเงา',
                                                    'price_carcare' => 175
                                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                                'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        ),
                                 new PostbackTemplateActionBuilder(
                                                'ล้างสี+ขัดเงา     250 B', // ข้อความแสดงในปุ่ม
                                                http_build_query(array(
                                                    'userid' => $userId,
                                                    'name_carcare' => 'promotion',
                                                    'type_carcare' => 'รถ 6 ล้อ',
                                                    'carnumber_carcare' => 'promotion',
                                                    'option_carcare' => 'ล้างสี+ขัดเงา',
                                                    'price_carcare' => 250
                                                )), // ข้อมูลที่จะส่งไปใน webhook ผ่าน postback event
                                                'ทำการจองเรียบร้อยแล้วครับ' // ข้อความที่จะแสดงฝั่งผู้ใช้ เมื่อคลิกเลือก
                                        ),
                            );
                            $replyData = new TemplateMessageBuilder('Carousel', new CarouselTemplateBuilder(
                                    array(
//                                new CarouselColumnTemplateBuilder(
//                                        'โปรโมชั่น เดือน สิงหาคม', 'ล้างรถ 10 ครั้ง ฟรี 1 ครั้ง', 'https://image.freepik.com/free-vector/modern-sale-banner-colorful-comic-style_1361-1314.jpg', $actionBuilder
//                                ),
                                new CarouselColumnTemplateBuilder(
                                        'โปรโมชั่น เดือน สิงหาคม ลด 50 %', 'รถ 6 ล้อ ', 'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg', $actionBuilder
                                ),
                                        new CarouselColumnTemplateBuilder(
                                        'โปรโมชั่น เดือน สิงหาคม ลด 50 %', 'รถ กะบะ ', 'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg', $actionBuilder
                                ),
                                        new CarouselColumnTemplateBuilder(
                                        'โปรโมชั่น เดือน สิงหาคม ลด 50 %', 'รถ เก๋ง ', 'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg', $actionBuilder
                                ),
                                        new CarouselColumnTemplateBuilder(
                                        'โปรโมชั่น เดือน สิงหาคม ลด 50 %', 'รถมอไซต์ ', 'https://image.freepik.com/free-vector/abstract-liquid-mega-sales-background_52683-18832.jpg', $actionBuilder
                                ),
                                    )
                                    )
                            );
                            break;


                        case "แผนที่":
                            $placeName = "คาร์แคร์ เซอร์วิส";
                            $placeAddress = "เลขที่ 36/6 เมือง, ถนนสุขุมวิท ตำบล ขลุง อำเภอ ขลุง จันทบุรี 22110";
                            $latitude = 12.461231;
                            $longitude = 102.228958;
                            $replyData = new LocationMessageBuilder($placeName, $placeAddress, $latitude, $longitude);
                            break;

                        case "ติดต่อเรา":
                            // กำหนด action 4 ปุ่ม 4 ประเภท
                            $actionBuilder = array(
//                                new UriTemplateActionBuilder(
//                                        'Facebook', // ข้อความแสดงในปุ่ม
//                                        'https://www.facebook.com/parin.zaa.39'
//                                ),
                                new UriTemplateActionBuilder(
                                        'โทร', // ข้อความแสดงในปุ่ม
                                        'tel://0981474661' // ข้อความแสดงในปุ่ม
                                )
                            );
                            $replyData = new TemplateMessageBuilder('Carousel', new CarouselTemplateBuilder(
                                    array(
                                new CarouselColumnTemplateBuilder(
                                        'ติดต่อเรา', 'เมืองขลุงคาร์แคร์', 'https://image.freepik.com/free-photo/businesswoman-looking-important-contact-phone_1163-4256.jpg', $actionBuilder
                                )
                                    )
                                    )
                            );
                            break;


                        case "y": // ส่วนทดสอบโต้ตอบข้อควมม flex
                            $response = $bot->getProfile($userId);
//                                    file_put_contents('log.txt',"userid : ".$userId . PHP_EOL, FILE_APPEND);
                            if ($response->isSucceeded()) {
                                $userData = $response->getJSONDecodedBody(); // return array     
                                $userId = $userData['userId'];
                            }
                             $result = $this->Carcare_model->read_carcare_by_id($userId);
                              file_put_contents('log.txt', "result : " . print_r($result, true) . PHP_EOL, FILE_APPEND);
                            $textReplyMessage = new BubbleContainerBuilder(
                                   
                                    "ltr", NULL, NULL, new BoxComponentBuilder(                 
                                    "vertical", array(
                                new TextComponentBuilder("Carcare Service"),
                                new TextComponentBuilder(" "),
                                new TextComponentBuilder("order"),
                                new TextComponentBuilder("date"),
                                new TextComponentBuilder("ชำระเงินแล้ว"),
                                new ImageComponentBuilder("https://dbbangalore.org/wp-content/uploads/2017/03/Blackline.png"),
                                new TextComponentBuilder("ITEMS"),
                                new TextComponentBuilder("ล้างรถ"),
                                new ImageComponentBuilder("https://dbbangalore.org/wp-content/uploads/2017/03/Blackline.png"),
                                new TextComponentBuilder("TOTAL"),
                                    )
                                    )
                            );
                            $replyData = new FlexMessageBuilder("This is a Flex Message", $textReplyMessage);
                            break;
                        default:
                            $textReplyMessage = "มีบริการล้างรถเมนูให้คุณเลือกพิมพ์ 5 เมนู  ลงทะเบียนรถ , จองคิว , โปรโมชั่น , แผนที่ , ติดต่อเรา เท่านี้ครับ ยินดีที่ให้บริการ";
                            $replyData = new TextMessageBuilder($textReplyMessage);
                            break;
                    }
                    break;

//                default:
//                    $textReplyMessage = json_encode($events);
//                    $replyData = new TextMessageBuilder($textReplyMessage);
//                    break;
            }
        }
//l ส่วนของคำสั่งตอบกลับข้อความ
        $response = $bot->replyMessage($replyToken, $replyData);
    }

}

?>