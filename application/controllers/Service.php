<?php
defined('BASEPATH') OR exit('NO direct script acces allowed');

class Service extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('service_model');
    }
    
    public function index(){
        $this->load->view('service');
    }
    public function create(){
        $name_carcare = $this->input->post('name_carcare');
        $phone_carcare = $this->input->post('phone_carcare');
        $option_carcare = $this->input->post('option_carcare');
        $type_carcare = $this->input->post('type_carcare');
        $message_carcare = $this->input->post('message_carcare');
        
        $savedata = array(
            'name_carcare' => $name_carcare,
            'phone_carcare' => $phone_carcare,
            'option_carcare' => $option_carcare,
            'type_carcare' => $type_carcare,
            'message_carcare' => $message_carcare
        );
    }
}