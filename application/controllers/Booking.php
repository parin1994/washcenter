<?php
defined('BASEPATH') or exit('NO direct script acces allowed');
class Booking extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('booking_model');
        $this->load->model('car_model');
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->model('carcarestore_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $this->load->helper('url');
        $data['store'] = $this->booking_model->read_booking_all();
        $this->load->view('booking', $data);
    }
    public function fetch_idcar()
    {
        $userid = $this->input->post('userid', TRUE);
        $data = $this->booking_model->read_booking_by_id3($userid);
        echo json_encode($data);
    }
    public function fetch_numbercar()
    {
        $numberId_car = $this->input->post('numberId_car', TRUE);
        $data = $this->booking_model->read_booking_namecar($numberId_car);
        echo json_encode($data);
    }
    public function fetch_date()
    {
        $email = $this->input->post('email', TRUE);
        $data = $this->booking_model->date($email);
        echo json_encode($data);
    }
    public function fetch_option()
    {
        $email = $this->input->post('email', TRUE);
        $data = $this->booking_model->read_booking_option($email);
        echo json_encode($data);
    }
    public function fetch_price()
    {
        $id_service = $this->input->post('id_service', TRUE);
        $data = $this->booking_model->read_booking_price($id_service);
        echo json_encode($data);
    }
    public function fetch_typeoption()
    {
        $email = $this->input->post('email', TRUE);
        $numberId_car = $this->input->post('numberId_car', TRUE);
        $data1 = $this->booking_model->read_carnumber($numberId_car);
        $resultObj1 = $this->ToObject($data1);
        foreach ($resultObj1 as $car) {
            $type_car = $car->type_car;
        }
        $data = $this->booking_model->typeoption($type_car,$email);
        echo json_encode($data);
    }
    public function create()
    {
        $userid = $this->input->post('userid');
        $data = $this->booking_model->read_name($userid);
        $resultObj = $this->ToObject($data);
        foreach ($resultObj as $name) {
            $name =  $name->name_register;
        }
        $data3 = $this->booking_model->read_name($userid);
        $resultObj4 = $this->ToObject($data3);
        foreach ($resultObj4 as $lastname) {
            $lastname =  $lastname->lastname_register;
        }
        $name_carcarestore = $this->input->post('store');
        
        $car_number = $this->input->post('numbercar');
        $data1 = $this->booking_model->read_carnumber($car_number);
        $resultObj1 = $this->ToObject($data1);
        foreach ($resultObj1 as $car) {
            $name_car = $car->name_car;
            $type_car = $car->type_car;
        }
       
        $option = $this->input->post('option');
        $data2 = $this->booking_model->read_option($option);
        $resultObj2 = $this->ToObject($data2);
        foreach ($resultObj2 as $car) {
            $nameoption = $car->option;
            $price = $car->price;
        }

        $total = $this->input->post('total');
        $time = $this->input->post('date');
        $date = $this->booking_model->read_date($time);
        $resultObj3 = $this->ToObject($date);
        foreach ($resultObj3 as $date) {
            $namedate = $date->date_date;
        }
        $id_receipt = $this->input->post('id_receipt');
        $object = new StdClass();
        $object->userid = $userid;
        $object->name = $name;
        $object->last_name = $lastname;
        $object->name_carcarestore = $name_carcarestore;
        $object->name_car = $name_car;
        $object->car_number = $car_number;
        $object->type_car = $type_car;
        $object->name_promotion = "-";
        $object->option = $nameoption;
        $object->price = $price;
        $object->total = $total;
        $object->time = $namedate;
        $object->id_date = $time;
        $object->date = date("Y/m/d H:i:s");
        $object->id_receipt = $id_receipt;
        $object->status = 1;
       
        $data['payment'] = $object;
        $data['read'] = $this->booking_model->read_booking_by_id_payment($name_carcarestore);
        $this->load->view('payment', $data);
        
    }
    public function payment()
    {
        $userid = $this->input->post('userid');
        $name = $this->input->post('name');
        $last_name = $this->input->post('last_name');
        $name_carcarestore = $this->input->post('name_carcarestore');
        $name_car = $this->input->post('name_car');
        $car_number = $this->input->post('car_number');
        $type_car = $this->input->post('type_car');
        $nameoption = $this->input->post('option');
        $price = $this->input->post('price');
        $total = $this->input->post('total');
        $id_date = $this->input->post('id_date');
        $time = $this->input->post('time');
        $id_receipt = $this->input->post('id_receipt');
        $img = $this->input->post('img_bank');


        $receipt = '';
        if (isset($_FILES['img']['name'])) {
            $config['upload_path'] = './assets/content';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '200';
            $this->load->library('upload', $config);
            $this->upload->do_upload('img');
            $source = './assets/content/' . $_FILES['img']['name'];

            $this->load->library('ftp');
            $ftp_config['hostname'] = '157.230.44.107';
            $ftp_config['username'] = 'appmoro_boteye';
            $ftp_config['password'] = 'qz8tXoa8r9';
            $ftp_config['debug']    = TRUE;

            //Connect to the remote server
            $this->ftp->connect($ftp_config);
            $this->ftp->upload($_FILES['img']['tmp_name'],"/public_html/assets/receipt/".$_FILES['img']['name'],"ascii", 0775);

            //Close FTP connection
            $this->ftp->close();
            $receipt = './assets/receipt/' . $_FILES['img']['name'];
        }
            $savedata = array(
                    'userid' => $userid,
                    'name' => $name,
                    'last_name' => $last_name,
                    'name_carcarestore' => $name_carcarestore,
                    'name_car' => $name_car,
                    'car_number' => $car_number,
                    'type_car' => $type_car,
                    'name_promotion' => "-",
                    'option' => $nameoption,
                    'price' => $price,
                    'total' => $price,
                    'time' => $time,
                    'id_date' => $id_date,
                    'date' => date("Y/m/d H:i:s"),
                    'id_receipt' => $id_receipt,
                    'img_receipt' => $receipt,
                    'status' => 1,
                );
                $result = $this->booking_model->create_booking($savedata);

                //$this->load->view('booking');
                echo "<script>
                alert('บันทึกข้อมูลสำเร็จ');
                window.location.href='https://boteye.appmoro.com/booking';
                </script>";
    }
    public function ToObject($Array)
    {
        $object = new stdClass();
        foreach ($Array as $key => $value) {
            if (is_array($value)) {
                $value = $this->ToObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }
}
