<?php
defined('BASEPATH') or exit('NO direct script acces allowed');
class Car extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('car_model');
    }
    public function index()
    {
        $data['readcar'] = $this->car_model->read_car();
        $this->load->view('car',$data);
    }
    public function create()
    {
        $userid = $this->input->post('userid');
        $numberId_car = $this->input->post('numberId_car');
        $name_car = $this->input->post('name_car');
        $type_car = $this->input->post('type_car');

        $row = $this->car_model->read_carid($numberId_car);
        $count = count($row);
        if($count==1){
            echo "<script>
        alert('ทะเบียนรถซ้ำ');
        window.location.href='https://boteye.appmoro.com/car';
        </script>";
        }else{
            $savedata = array(
            'userid' => $userid,
            'numberId_car' => $numberId_car,
            'name_car' => $name_car,
            'type_car' => $type_car,
        );

        $result = $this->car_model->create_car($savedata);
        echo "<script>
        alert('ลงทะเบียนสำเร็จ');
        window.location.href='https://boteye.appmoro.com/car';
        </script>";
        }
        
    }
}
