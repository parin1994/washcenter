<?php
defined('BASEPATH') OR exit('NO direct script acces allowed');

class Register extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('register_model');
    }
    public function index(){
        $data['title'] = 'View Register Form';
        $data['read'] = $this->register_model->read_register_all();
        $data['aside'] = $this->load->view('carcareoffice/include/aside', '', TRUE);
        $data['header'] = $this->load->view('carcareoffice/include/header', '', TRUE);
        $this->load->view('carcareoffice/register/index', $data);
    }
    public function update($id_carcare){
        $data['title'] = 'Update Register Form';
        $data['method'] = 'Update';
        $data['header'] = $this->load->view('carcareoffice/include/header', '', TRUE);
        $data['aside'] = $this->load->view('carcareoffice/include/aside', '', TRUE);
        $data['con'] = $this->register_model->read_register_by_id($id_carcare);
        $method = $this->input->post('method');
        
         if (!empty($method)) {
            $name_carcarestore = $this->input->post('name_carcarestore');
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $tel = $this->input->post('tel');
           
            $savedata = array(
                'name_carcarestore' => $name_carcarestore,
                'name' => $name,
                'address' => $address,
                'tel' => $tel
            );
  
            $result = $this->register_model->update_register($savedata);
            redirect('carcareoffice/register/index');
        }

        $this->load->view('carcareoffice/register/form', $data);
    }
}
