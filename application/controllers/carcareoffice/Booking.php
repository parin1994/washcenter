<?php
defined('BASEPATH') OR exit('NO direct script acces allowed');

class Booking extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('booking_model');
        $this->load->model('carcarestore_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $email = $this->session->userdata('email');
        if (empty($email)) {
            redirect('carcareoffice/login');
        }
    }
    public function index(){
        $email = $this->session->userdata('email');
        $data['title'] = 'การจองคิว';
        $data['read'] = $this->booking_model->read_booking_by_id($email);
        $data['readname'] = $this->booking_model->read_namestore($email);
        $data['menu'] = $this->load->view('carcareoffice/include/menu', $data, TRUE);
        $data['headers'] = $this->load->view('carcareoffice/include/headers', '', TRUE);
        $this->load->view('carcareoffice/booking/index', $data);
    }
    public function update($queue)
    {
        $email = $this->session->userdata('email');
        $data['readname'] = $this->booking_model->read_namestore($email);
        $data['title'] = 'แก้ไขข้อมูล';
        $data['method'] = 'Update';
        $data['headers'] = $this->load->view('carcareoffice/include/headers', '', TRUE);
        $data['menu'] = $this->load->view('carcareoffice/include/menu',$data, TRUE);

        $data['con'] = $this->booking_model->read_booking_by_id1($queue);
        $method = $this->input->post('method');

        if (!empty($method)) {
            $date = $this->input->post('date');

            $savedata = array(
                'date' => $date

            );

            $result = $this->booking_model->update_booking($savedata);
            redirect('carcareoffice/booking/index');

            $data['con'] = $this->booking_model->read_booking_by_id($queue);
        }
        $this->load->view('carcareoffice/booking/form', $data);
    }
}
