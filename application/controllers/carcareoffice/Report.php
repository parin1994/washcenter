<?php
defined('BASEPATH') or exit('NO direct script acces allowed');
class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('report_model');
        $this->load->model('car_model');
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->model('carcarestore_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $email = $this->session->userdata('email');
        $date = date("Y-m-d");
        $data['date'] = $date;
        $data['title'] = 'ออกรายงาน';
        $data['report'] = $this->report_model->read_price_by_id_date($email,$date);
        $data['sum'] = $this->report_model->sum($email,$date);
        // print_r($data['sum']);
        // exit;
        $data['store'] = $this->report_model->read_carcarestore_by_id($email);
        $this->load->view('carcareoffice/booking/report', $data);
    }
    public function search(){
        $email = $this->session->userdata('email');
        $date = $this->input->post('date');
        $data['date'] = $date;
        $data['title'] = 'ออกรายงาน';
        $data['report'] = $this->report_model->read_price_by_id_date($email,$date);
        $data['sum'] = $this->report_model->sum($email,$date);
        $data['store'] = $this->report_model->read_carcarestore_by_id($email);
        $this->load->view('carcareoffice/booking/report', $data);
         }
}
