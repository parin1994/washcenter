<?php
defined('BASEPATH') or exit('NO direct script acces allowed');

class Promotion extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('upload');
        $this->load->model('promotion_model');
        $this->load->model('carcarestore_model');
        $this->load->model('booking_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $email = $this->session->userdata('email');

        if (empty($email)) {
            redirect('carcareoffice/login');
        }
    }
    public function index()
    {
        $email = $this->session->userdata('email');
        $data['title'] = 'โปรโมชัน';
        $data['readname'] = $this->booking_model->read_namestore($email);
        $data['headers'] = $this->load->view('carcareoffice/include/headers', '', TRUE);
        $data['menu'] = $this->load->view('carcareoffice/include/menu', $data, TRUE);
        $data['read'] = $this->promotion_model->read_promotion_by_id($email);
        // $data['read'] = $this->service_model->read_service_all();
        $this->load->view('carcareoffice/promotion/index', $data);
    }
    public function create()
    {
        $data['title'] = 'เพิ่มข้อมูล';
        $data['method'] = 'Create';
        $email = $this->session->userdata('email');
        $data['readname'] = $this->booking_model->read_namestore($email);
        $data['headers'] = $this->load->view('carcareoffice/include/headers', '', TRUE);
        $data['menu'] = $this->load->view('carcareoffice/include/menu',  $data, TRUE);
        $method = $this->input->post('method');


        $data['con'] = new stdClass();
        $data['con']->id_promotion = '';
        $data['con']->name_pro = '';
        $data['con']->img_pro = '';
        $data['con']->option_pro = '';
        $data['con']->price_pro = '';
        $data['con']->date_pro = '';
        $data['con']->date_out = '';
        $email = $this->session->userdata('email');

        $name_pro = $this->input->post('name_pro');
        if (!empty($name_pro)) {
            $option_pro = $this->input->post('option_pro');
            $price_pro = $this->input->post('price_pro');
            $date_pro = $this->input->post('date_pro');
            $date_out = $this->input->post('date_out');

            $savedata = array(
                'name_pro' => $name_pro,
                'option_pro' => $option_pro,
                'price_pro' => $price_pro,
                'date_pro' => $date_pro,
                'date_out' => $date_out,
                'email' => $email

            );
            $result = $this->promotion_model->create_promotion($savedata);
            redirect('carcareoffice/promotion/index');
        }
        $this->load->view('carcareoffice/promotion/form', $data);
    }

    public function update($id_promotion)
    {
        $email = $this->session->userdata('email');
        $data['readname'] = $this->booking_model->read_namestore($email);
        $data['title'] = 'แก้ไขข้อมูล';
        $data['method'] = 'Update';
        $data['headers'] = $this->load->view('carcareoffice/include/headers', '', TRUE);
        $data['menu'] = $this->load->view('carcareoffice/include/menu',  $data, TRUE);
        $data['con'] = $this->promotion_model->read_promotion_by_id1($id_promotion);

        $method = $this->input->post('method');

        if (!empty($method)) {
            $name_pro = $this->input->post('name_pro');
            $option_pro = $this->input->post('option_pro');
            $price_pro = $this->input->post('price_pro');
            $date_pro = $this->input->post('date_pro');
            $date_out = $this->input->post('date_out');

            $savedata = array(
                'id_promotion' => $id_promotion,
                'name_pro' => $name_pro,
                'option_pro' => $option_pro,
                'price_pro' => $price_pro,
                'date_pro' => $date_pro,
                'date_out' => $date_out,


            );
            $result = $this->promotion_model->update_promotion($savedata);
            redirect('carcareoffice/promotion/index');

            $data['con'] = $this->promotion_model->read_promotion_by_id($id_promotion);
        }
        $this->load->view('carcareoffice/promotion/form', $data);
    }


    public function delete($id_promotion)
    {

        if (!empty($id_promotion)) {

            $result = $this->promotion_model->delete_promotion($id_promotion);
        }

        $response = array('Delete SUCESS');
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response))
            ->_display();
        exit;
    }
}
