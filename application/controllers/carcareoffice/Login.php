<?php
defined('BASEPATH') OR exit('NO direct script acces allowed');
class Login extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('carcarestore_model');
        $this->load->library('session');
    }
    public function index(){
        $this->load->view('carcareoffice/login/loginn');
    }
    public function auth() {
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');

        if (empty($email)) {
            $http_status = 400;
            $response = array('message' => 'ระบุ email');
        } else if (empty($pass)) {
            $http_status = 400;
            $response = array('message' => 'ระบุ password');
        } else {
            $this->load->model('carcarestore_model');
            $member = $this->carcarestore_model->read_member_by_email_and_pass($email, $pass);
            

            if (!empty($member)) {
                $http_status = 200;
                $response = array('message' => 'login successfully');
                
                
                $this->session->set_userdata('email', $member[0]->email);
                redirect('carcareoffice/booking');
                
            } else {
               // $http_status = 400;
               // $response = array('message' => 'email or password ไม่ถูกต้อง');
               ?>
               <script type="text/javascript">
               alert("email or password ไม่ถูกต้อง");
               location.href="index";
               </script>
               <?php
            }
        }
        $this->output
                ->set_status_header($http_status)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
        ;
    }
    public function logout(){
       
        $this->session->unset_userdata('email','');
        $this->session->sess_destroy();
            redirect('carcareoffice/login');
        }
}