<?php
defined('BASEPATH') or exit('NO direct script acces allowed');

class Showbooking extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->model('showbooking_model');
        
    }
    public function index()
    {
        $data['title'] = 'ประวัติการจอง';
        $data['header_cus'] = $this->load->view('carcareoffice/include/header_cus', '', TRUE);
        
        $this->load->view('showbooking',$data);
    }
    public function request()
    {
        $userid = $this->input->post('userid');
        if ($userid) {
            $data = $this->showbooking_model->read_showbooking_by_id($userid);
            echo json_encode($data);
        }
    }
   }

