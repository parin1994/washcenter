<!DOCTYPE html>
<html lang="en">
<!-- <script src="https://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/vconsole/3.0.0/vconsole.min.js"></script>
        <script> var vConsole = new VConsole();</script> -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Showbooking</title>

    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="<?php echo base_url('assets/services/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/services/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url('assets/services/lib/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/css/zabuto_calendar.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/lib/gritter/css/jquery.gritter.css') ?>" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/services/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/services/css/style-responsive.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/services/lib/chart-master/Chart.js') ?>"></script>
</head>

<body>
    <section id="container">

        <?php echo $header_cus; ?>

        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9 main-chart">
                        <!--CUSTOM CHART START -->
                        <div class="border-head">
                            <h3><?php echo $title ?> <br></h3>
                                <input type="hidden" id="userid" name="userid" value="">
                            <br>
                        </div>
                        <br>
                        <div style="overflow-x:auto;">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                    <th style="width:10%;text-align:center">ชื่อร้าน</th>
                                        <th style="width:20%;text-align:center">ยี่ห้อรถ</th>
                                        <th style="width:20%;text-align:center">ทะเบียนรถ</th>
                                        <th style="width:20%;text-align:center">ประเภทรถ</th>
                                        <th style="width:10%;text-align:center">โปรโมชัน</th>
                                        <th style="width:20%;text-align:center">ประเภทการล้าง</th>
                                        <th style="width:10%;text-align:center">วันที่</th>
                                        <th style="width:10%;text-align:center">เวลา</th>
                                        <th style="width:20%;text-align:center">ยอดสุทธิ</th>
                                    </tr>
                                </thead>
                                <tbody id="table">
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>

                </div>

            </section>
        </section>


        <!--footer end-->
    </section>


    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/services/lib/jquery/jquery.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/services/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/services/lib/jquery.dcjqaccordion.2.7.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.scrollTo.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.nicescroll.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.sparkline.js') ?>"></script>
    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/services/lib/common-scripts.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter/js/jquery.gritter.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter-conf.js') ?>"></script>
    <!--script for this page-->
    <script src="<?php echo base_url('assets/services/lib/sparkline-chart.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/zabuto_calendar.js') ?>"></script>

    <script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
    <script src="liff-starter.js"></script>
    <script>
        window.onload = function(e) {
            liff.init(function(data) {
                runApp();
            });
        };

        function runApp() {
            liff.getProfile().then(profile => {
                let userid = document.getElementById("userid").value = profile.userId;
                $.ajax({
                url: "<?php echo site_url('showbooking/request'); ?>",
                method: "POST",
                data: {
                    userid: userid
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    console.log(data)
                    var html = '';
                    var i;
                        data.forEach(v => {
                            html += '<tr><td style="width:10%;text-align:center">'+v.name_carcarestore+'</td><td style="width:50px;text-align:center">'+v.name_car+'</td><td style="width:20%;text-align:center">'+v.car_number+'</td><td style="width:10%;text-align:center">'+v.type_car+'</td><td style="width:10%;text-align:center">'+v.name_promotion+'</td><td style="width:20%;text-align:center">'+v.option+'</td><td style="width:10%;text-align:center">'+v.date+'</td><td style="width:10%;text-align:center">'+v.time+'</td><td style="width:20%;text-align:right">'+v.total+' บาท'+'</td><tr>';
                        })
                    $('#table').html(html);
                }

                
            });
                

            }).catch(err => console.log('ERROR MESSEGE:'+err));
        }

        liff.init({
            liffId: "1653826205-RvdGdXwZ"
        }, () => {
            if (liff.isLoggedIn()) {
                runApp()
            } else {
                liff.login();
            }
        }, err => console.error(err.code, error.message));
    </script>

</body>

</html>