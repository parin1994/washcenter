
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Carcarestore</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="<?php echo base_url('assets/register/images/icons/favicon.ico') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/bootstrap/css/bootstrap.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/animate/animate.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/css-hamburgers/hamburgers.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/animsition/css/animsition.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/select2/select2.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/daterangepicker/daterangepicker.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/css/util.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/css/main.css') ?>">
        <style type="text/css">
        .long {
	font-size: 16px;
	color: #FF0000;
}
.contact {
  width: 100%;
  display: block;
  font-family: Poppins-Regular;
  font-size: 18px;
  color: #333333;
  line-height: 1.2;
  padding-bottom: 10px;
  padding-top: 20px;
}
</style>
    </head>
    <body>
        <div class="container-contact100" style="background-color:darkblue;">
            <div class="wrap-contact100">
                    <form action="<?php echo base_url('carcarestore/create') ?>" method="post" class="contact100-form validate-form" style="width:100%;"  enctype="multipart/form-data">
                    <span class="contact100-form-title">
                        ลงทะเบียนร้านบริการ
                    </span>
                    <input type="hidden" id="userid_carcarestore" name="userid_carcarestore" value="">
                    <label class="label-input100" ><h3><i class="fa fa-home" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกชื่อร้านบริการด้วยครับ">
                        <input id="name_carcarestore" class="input100" type="text" name="name_carcarestore" placeholder="ชื่อ ร้านค้า">
                        <span class="focus-input100"></span>
                    </div>
                    <span class="contact">
                    รูปภาพ
                    </span>
                    <input type="file" name="img" id="img" >

                    <label class="label-input100"><h3><i class="fa fa-user" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="กรุณากรอกชื่อครับ">
                        <input id="firstname" class="input100" type="text" name="firstname" placeholder="ชื่อ">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 rs2-wrap-input100 validate-input" data-validate="กรุณากรอกนามสกุลครับ">
                        <input id="lastname" class="input100" type="text" name="lastname" placeholder="นามสกุล">
                        <span class="focus-input100"></span>
                    </div>
                    <label class="label-input100" for="phone"><h3><i class="fa fa-mobile" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกหมายเลขโทรศัพท์ครับ">
                        <input id="tel" class="input100" type="tel" name="tel" maxlength="10" placeholder="เบอร์โทรศัพท์">
                        <span class="focus-input100"></span>
                    </div>
                    <label class="label-input100" for="email"><h3><i class="fa fa-envelope" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกอีเมลครับ">
                        <input id="email" class="input100" type="email" name="email" placeholder="อีเมล">
                        <span class="focus-input100"></span>
                    </div>
                    <label class="label-input100" ><h3><i class="fa fa-unlock-alt" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกรหัสผ่านด้วยครับ">
                        <input id="pass" class="input100" type="text" name="pass" placeholder="รหัสผ่าน">
                        <span class="focus-input100"></span>
                    </div>
                    <label class="label-input100"><h3><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="กรุณากรอกเส้นละติจูดด้วยครับ">
                        <input id="lat" class="input100" type="text" name="lat" placeholder="ละติจูด">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 rs2-wrap-input100 validate-input" data-validate="กรุณากรอกเส้นลองจิจูดด้วยครับ">
                        <input id="lng" class="input100" type="text" name="lng" placeholder="ลองจิจูด">
                        <span class="focus-input100"></span>
                    </div>
                   <a class="long" href = "https://www.booster4web.com/map-location/">คลิกลิ้งค์เพื่อเข้าไปหาเส้น ละติจูด,ลองจิจูด</a>
                    <label class="label-input100" for="message"><h3><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกที่อยู่ครับ">
                        <textarea id="address" class="input100" name="address" placeholder="ที่อยู่"></textarea>
                        <span class="focus-input100"></span>
                    </div>
                    <label class="label-input100" for="phone"><h3><i class="fa fa-mobile" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกหมายเลขบัญชีธนาคาร">
                        <input id="banknumber" class="input100" type="text" name="banknumber" placeholder="เลขบัญชีธนาคาร">
                        <span class="focus-input100"></span>
                    </div>
                    <span class="contact">
                    <label for="InputBranch">เลือกธนาคาร<span>*</span></label>
                    <div class="wrap-input100 validate-input">
                    <select name="bank" id="bank" class="form-control required">
                            <option selected="" disabled="">ธนาคาร</option>
                            <?php foreach ($bank as $value) { ?>
                                <option value='<?php echo $value->name_bank ?>'><?php echo $value->name_bank ?></option>
                            <?php } ?>
                        </select>
                        <span class="focus-input100"></span>
                    </div>
                    <span class="contact">
                    <label class="label-input100" for="phone"><h3><i class="fa fa-mobile" aria-hidden="true"></i></h3></label>
                    <div class="wrap-input100 validate-input" data-validate="กรุณากรอกชื่อบัญชีธนาคาร">
                        <input id="bankname" class="input100" type="text" name="bankname" placeholder="ชื่อบัญชีธนาคาร">
                        <span class="focus-input100"></span>
                    </div>
                    <span class="contact">
                    รูปภาพ QRCODE ธนาคาร
                    </span>
                    <input type="file" name="img_bank" id="img_bank" >
                    <div class="container-contact100-form-btn">
                        <button  class="contact100-form-btn">
                            บันทึก
                        </button>                                       
                    </div>
                </form>
            </div>
        </div>
        <div id="dropDownSelect1"></div>

        <script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
        <script src="<?php echo base_url('assets/register/vendor/jquery/jquery-3.2.1.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

        <script src="<?php echo base_url('assets/register/vendor/animsition/js/animsition.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

        <script src="<?php echo base_url('assets/register/vendor/bootstrap/js/popper.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
        <script src="<?php echo base_url('assets/register/vendor/bootstrap/js/bootstrap.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

        <script src="<?php echo base_url('assets/register/vendor/select2/select2.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
        <script type="3d44b465189b22b734a3929d-text/javascript">
            $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
            });
        </script>

        <script src="<?php echo base_url('assets/register/vendor/daterangepicker/moment.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
        <script src="<?php echo base_url('assets/register/vendor/daterangepicker/daterangepicker.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

        <script src="<?php echo base_url('assets/register/vendor/countdowntime/countdowntime.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

        <script src="<?php echo base_url('assets/register/js/main.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="3d44b465189b22b734a3929d-text/javascript"></script>
        <script type="3d44b465189b22b734a3929d-text/javascript">
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-23581568-13');
        </script>
        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="3d44b465189b22b734a3929d-|49" defer=""></script></body>
    <script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
    <script src="liff-starter.js"></script>
    <script>
        window.onload = function (e) {
            liff.init(function (data) {
                initializeApp(data);
            });
        };

        function initializeApp(data) {
            document.getElementById('userid_carcarestore').value = data.context.userId;
        }
    </script>
</html>
