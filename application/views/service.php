
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Contact V18</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="<?php echo base_url('assets/carcare/images/icons/favicon.ico') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/vendor/bootstrap/css/bootstrap.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') ?>">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/vendor/animate/animate.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/vendor/css-hamburgers/hamburgers.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/vendor/animsition/css/animsition.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/vendor/select2/select2.min.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/vendor/daterangepicker/daterangepicker.css') ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/css/util.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/carcare/css/main.css') ?>">

    </head>
    <body>
        <div class="container-contact100">
            <div class="wrap-contact100">
                <form class="contact100-form validate-form">
                    <span class="contact100-form-title">
                        จองคิวล้างรถ
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Name is required">
                        <label class="label-input100" for="name">Full name</label>
                        <input id="name" class="input100" type="text" name="name" placeholder="Enter your name...">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Phone is required">
                        <label class="label-input100" for="email">Phone</label>
                        <input id="tell" class="input100" type="text" name="tell" placeholder="Enter your Phone...">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100">
                        <div class="label-input100">What do you need?</div>
                        <div>
                            <select class="js-select2" name="service">
                                <option disabled="" selected="">category</option>
                                <option>ล้างอัดฉีด</option>
                                <option>ล้างสี+ดูดฝุ่น</option>
                                <option>ล้าง+พ่นน้ำมันกันสนิม</option>
                                <option>ล้าง+ขัดเงา</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100">
                        <div class="label-input100">What do you need?</div>
                        <div>
                            <select class="js-select2" name="service">
                                <option disabled="" selected="">car category</option>
                                <option>รถ 6 ล้อ</option>
                                <option>รถตู้</option>
                                <option>รถกระบะ</option>
                                <option>รถเก๋ง</option> 
                                <option>มอไซต์</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Name is required">
                        <label class="label-input100" for="name">Price</label>
                        <input id="" class="input100" type="text" name="" placeholder="">
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Message is required">
                        <label class="label-input100" for="message">Message</label>
                        <textarea id="message" class="input100" name="message" placeholder="Type your message here..."></textarea>
                        <span class="focus-input100"></span>
                    </div>
                    <div class="container-contact100-form-btn">
                        <button class="contact100-form-btn">
                            Send
                        </button>
                    </div>
<!--                    <div class="contact100-form-social flex-c-m">
                        <a href="#" class="contact100-form-social-item flex-c-m bg1 m-r-5">
                            <i class="fa fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="contact100-form-social-item flex-c-m bg2 m-r-5">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="contact100-form-social-item flex-c-m bg3">
                            <i class="fa fa-youtube-play" aria-hidden="true"></i>
                        </a>
                    </div>-->
                </form>
                <div class="contact100-more flex-col-c-m" style="background-image: url(<?php echo base_url('assets/carcare/images/bg-01.jpg') ?>);">
                </div>
            </div>
        </div>

        <script src="<?php echo base_url('assets/carcare/vendor/jquery/jquery-3.2.1.min.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>

        <script src="<?php echo base_url('assets/carcare/vendor/animsition/js/animsition.min.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>

        <script src="<?php echo base_url('assets/carcare/vendor/bootstrap/js/popper.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>
        <script src="<?php echo base_url('assets/carcare/vendor/bootstrap/js/bootstrap.min.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>

        <script src="<?php echo base_url('assets/carcare/vendor/select2/select2.min.js" type="427b9914d790507c21d1f0ad-text/javascript') ?>"></script>
        <script type="427b9914d790507c21d1f0ad-text/javascript">
            $(".js-select2").each(function(){
            $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
            });
            })
            $(".js-select2").each(function(){
            $(this).on('select2:open', function (e){
            $(this).parent().next().addClass('eff-focus-selection');
            });
            });
            $(".js-select2").each(function(){
            $(this).on('select2:close', function (e){
            $(this).parent().next().removeClass('eff-focus-selection');
            });
            });

        </script>

        <script src="<?php echo base_url('assets/carcare/vendor/daterangepicker/moment.min.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>
        <script src="<?php echo base_url('assets/carcare/vendor/daterangepicker/daterangepicker.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>

        <script src="<?php echo base_url('assets/carcare/vendor/countdowntime/countdowntime.js') ?>" type="427b9914d790507c21d1f0ad-text/javascript"></script>

        <script src="<?php echo base_url('assets/carcare/js/main.js" type="427b9914d790507c21d1f0ad-text/javascript') ?>"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="427b9914d790507c21d1f0ad-text/javascript"></script>
        <script type="427b9914d790507c21d1f0ad-text/javascript">
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-23581568-13');
        </script>
        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="427b9914d790507c21d1f0ad-|49" defer=""></script></body>
</html>
