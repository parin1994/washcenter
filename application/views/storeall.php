<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Store</title>

    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <link href="<?php echo base_url('assets/services/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/services/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url('assets/services/lib/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/css/zabuto_calendar.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/lib/gritter/css/jquery.gritter.css') ?>" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/services/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/services/css/style-responsive.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/services/lib/chart-master/Chart.js') ?>"></script>
</head>

<body>
    <section id="container">
 
        <?php echo $header_cus; ?>

        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9 main-chart">
                        <!--CUSTOM CHART START -->
                        <div class="border-head">
                            <h3><?php echo $title ?> <br></h3>
                            <form action="<?php echo base_url('storeall/search') ?>" method="post" enctype="multipart/form-data"> 
                                <input type="text" name="title" id="title">
                                <button type="submit"> ค้นหา </button>
                            </form>
                            <br>
                            <a href="<?php echo base_url('storeall') ?>">
                            <button>ร้านทั้งหมด</button>
                            </a>
                        </div>
                        <br>
                        <div style="overflow-x:auto;">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ชื่อร้านค้า</th>
                                        <th>เบอร์โทร</th>
                                        <th>ที่อยู่</th>
                                    </tr>


                                </thead>
                                <?php foreach ($read as $value) { ?>
                                    <tbody>
                                        <tr>

                                            <td><?php echo $value->name_carcarestore ?></td>
                                            <td><?php echo $value->tel ?></td>
                                            <td><?php echo $value->address ?></td>

                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                               
                            </table>
                        </div>
                    </div>
                </div>

                </div>

            </section>
        </section>


        <!--footer end-->
    </section>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                ยืนยันการลบ
                </div>
                <div class="modal-body">
                    คุณต้องการลบหรือไม่ ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <a class="btn btn-danger btn-ok clickDelete">ลบ</a>
                </div>
            </div>
        </div>
    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/services/lib/jquery/jquery.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/services/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/services/lib/jquery.dcjqaccordion.2.7.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.scrollTo.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.nicescroll.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.sparkline.js') ?>"></script>
    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/services/lib/common-scripts.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter/js/jquery.gritter.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter-conf.js') ?>"></script>
    <!--script for this page-->
    <script src="<?php echo base_url('assets/services/lib/sparkline-chart.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/zabuto_calendar.js') ?>"></script>

    <script type="application/javascript">
        $(document).ready(function() {
            $("#date-popover").popover({
                html: true,
                trigger: "manual"
            });
            $("#date-popover").hide();
            $("#date-popover").click(function(e) {
                $(this).hide();
            });

            $("#my-calendar").zabuto_calendar({
                action: function() {
                    return myDateFunction(this.id, false);
                },
                action_nav: function() {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [{
                        type: "text",
                        label: "Special event",
                        badge: "00"
                    },
                    {
                        type: "block",
                        label: "Regular event",
                    }
                ]
            });
        });

        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
        if (window.self == window.top) {
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-55234356-6', 'auto');
            ga('send', 'pageview');
        }
    </script>

    
</body>

</html>