<!DOCTYPE html>
<html lang="en">
<script src="https://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/vconsole/3.0.0/vconsole.min.js"></script>
        <script> var vConsole = new VConsole();</script>
<head>
    <title>Booking</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="<?php echo base_url('assets/register/images/icons/favicon.ico') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/bootstrap/css/bootstrap.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/animate/animate.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/css-hamburgers/hamburgers.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/animsition/css/animsition.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/select2/select2.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/daterangepicker/daterangepicker.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/css/util.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/css/main.css') ?>">



</head>
<style>
    .js-select2 {
        font-family: Poppins-Regular;
        font-size: 15px;
        color: #555555;
        text-transform: uppercase;
        letter-spacing: 1px;

        width: 450%;
        min-height: 55px;
        border: 1px solid #e6e6e6;
        margin-top: 0px;
        line-height: 1.2;
        padding: 0 25px;

        margin-bottom: 0;
    }

    .label-input150 {
        align-items: center;
        font-family: Poppins-Regular;
        font-size: 12px;
        color: #555555;
        line-height: 1.5;
        text-transform: uppercase;
        letter-spacing: 1px;

        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;

        width: 100%;
        min-height: 55px;
        border: 1px solid #e6e6e6;
        padding: 10px 25px;
        margin-top: 15px;
        margin-bottom: 0;
    }
</style>

<body>

    <div class="container-contact100" style="background-color:darkblue;">
        <div class="card col-10 col-md-6 offset-md-2">
            <center>
                <br></br>
                <img id="pictureUrl" width="30%">
                <br></br>
                <h6 id="displayName"></h6>
                <!-- <h6 id ="userid"></h6> -->
                <br></br>
            </center>
        </div>

        <div class="card col-10 col-md-6 offset-md-2">
            <form action="<?php echo base_url('booking/create') ?>" method="post" class="contact100-form validate-form" style="width:100%;">
                <span class="col-12 text-center">
                    <h5>จองคิวล้างรถ</h5>
                </span>
                <br></br>
                <input type="hidden" id="userid" name="userid" value="">

                <div class="form-group col-12">
                    <label for="InputBranch">ค้นหาร้านใกล้คุณ<span>*</span></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <select name="store" id="store" class="form-control required" required>
                            <option selected="" disabled="">เลือกร้าน</option>
                            <?php foreach ($store as $value) { ?>
                                <option value='<?php echo $value->email ?>'><?php echo $value->name_carcarestore ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-12">
                    <label for="InputBranch">เลือกทะเบียนรถ<span>*</span></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <select name="numbercar" id="numbercar" class="form-control required" required>
                            <option selected="" disabled="">ทะเบียนรถ</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-12">
                    <select name="name_car" id="name_car" class="form-control required" disabled="" required>
                        <option selected="" disabled="">ยี่ห้อรถ</option>
                    </select>
                </div>
                <div class="form-group col-12">
                    <select name="typecar" id="typecar" class="form-control required" disabled="" required>
                        <option selected="" disabled="">ขนาดรถ</option>
                    </select>
                </div>
                <div class="form-group col-12">
                    <select name="option" id="option" class="form-control required" required>
                        <option selected="" disabled="">ประเภทการล้าง</option>
                    </select>
                </div>
                <div class="form-group col-12">
                    <select name="price" id="price" class="form-control required" disabled="" required>
                        <option selected="" disabled="">ราคา</option>
                    </select>
                </div>
                <div class="form-group col-12">
                    <label for="InputBranch">เลือกเวลา<span>*</span></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <select name="date" id="date" class="form-control required" required>
                            <option selected="" disabled="">เวลา</option>
                        </select>
                    </div>
                </div>
                <div class="container-contact100-form-btn">
                    <button href="<?php echo base_url('booking') ?>" class="contact100-form-btn">
                        หน้าถัดไป
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="dropDownSelect1"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
    <script src="<?php echo base_url('assets/register/vendor/jquery/jquery-3.2.1.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/animsition/js/animsition.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/bootstrap/js/popper.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script src="<?php echo base_url('assets/register/vendor/bootstrap/js/bootstrap.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/select2/select2.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script type="3d44b465189b22b734a3929d-text/javascript">
        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>

    <script src="<?php echo base_url('assets/register/vendor/daterangepicker/moment.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script src="<?php echo base_url('assets/register/vendor/daterangepicker/daterangepicker.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/countdowntime/countdowntime.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/js/main.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script type="3d44b465189b22b734a3929d-text/javascript">
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="3d44b465189b22b734a3929d-|49" defer=""></script>
</body>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script src="liff-starter.js"></script>
<script>
    window.onload = function(e) {
        liff.init( function(data) {
             runApp();
        });
    };

    function runApp() {
        liff.getProfile().then(profile => {
            document.getElementById("pictureUrl").src = profile.pictureUrl;
            document.getElementById("displayName").innerHTML = '<b>สวัสดีคุณ </b> ' + profile.displayName;
            document.getElementById("userid").value = profile.userId;
            
        }).catch(err => console.error(err));
    }
    
    liff.init({
        liffId: "1653826205-ge9k965J"
    }, () => {
        if (liff.isLoggedIn()) {
            runApp()
        } else {
            liff.login();
        }
    }, err => console.error(err.code, error.message));
</script>
<script>
    $(document).ready(function() {
        
        $('#store').change(function() {
            var email = $(this).val();
            $.ajax({
                url: "<?php echo site_url('booking/fetch_option'); ?>",
                method: "POST",
                data: {
                    email: email
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    html = '<option  selected="" disabled="">ประเภทการล้าง</option>'
                    p = '<option  selected="" disabled="">ราคา</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_service + '>' + data[i].option + '</option>';
                    }
                    // $('#option').html(html);
                    // $('#price').html(p);
                }
            });
            $.ajax({
                url: "<?php echo site_url('booking/fetch_date'); ?>",
                method: "POST",
                data: {
                    email: email
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    html = '<option selected="" disabled="">เวลา</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_date + '>' + data[i].date_date + '</option>';
                    }
                    $('#date').html(html);
                }
            });
            getdata()
            function getdata() {
            let userid = document.getElementById("userid").value
            $.ajax({
                url: "<?php echo site_url('booking/fetch_idcar'); ?>",
                method: "POST",
                data: {
                    userid: userid
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    
                    var html;
                    var i;
                    var c ;
                    var h;
                    html = '<option selected="" disabled="">เลือกทะเบียนรถ</option>';
                    c = '<option selected="" disabled="">ยี่ห้อรถ</option>';
                    h = '<option selected="" disabled="">ประเภทรถ</option>';
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].numberId_car + '>' + data[i].numberId_car + '</option>';
                        c += '<option value=' + data[i].name_car + '  disabled="">' + data[i].name_car + '</option>';
                        h += '<option value=' + data[i].type_car + ' disabled="" >' + data[i].type_car + '</option>';
                    }
                    $('#numbercar').html(html);
                    $('#name_car').html(c);
                    $('#typecar').html(h);

                }
            });
        }
        });
        $('#numbercar').change(function() {
            var numberId_car = $(this).val();
            
            $.ajax({
                url: "<?php echo site_url('booking/fetch_numbercar'); ?>",
                method: "POST",
                data: {
                    numberId_car: numberId_car
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var c;
                    var h;
                    var i;
                    for (i = 0; i < data.length; i++) {
                        c += '<option value=' + data[i].name_car + '  disabled="">' + data[i].name_car + '</option>';
                        h += '<option value=' + data[i].type_car + ' disabled="" >' + data[i].type_car + '</option>';
                    }
                    $('#name_car').html(c);
                    $('#typecar').html(h);
                }
            });
            var email = document.getElementById('store').value
            var numberId_car = $(this).val();
            // alert(numberId_car);
            $.ajax({
                url: "<?php echo site_url('booking/fetch_typeoption'); ?>",
                method: "POST",
                data: {
                    email: email,
                    numberId_car: numberId_car
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    html = '<option  selected="" disabled="">ประเภทการล้าง</option>'
                    p = '<option  selected="" disabled="">ราคา</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_service + '>' + data[i].option + '</option>';
                    }
                    $('#option').html(html);
                    $('#price').html(p);
                }
            });
            return false;
        });
        $('#option').change(function() {
            var id_service = $(this).val();
            $.ajax({
                url: "<?php echo site_url('booking/fetch_price'); ?>",
                method: "POST",
                data: {
                    id_service: id_service
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_service + '>' + data[i].price + ' บาท</option>';
                    }
                    $('#price').html(html);
                }
            });
            return false;
        });
    });
</script>

</html>