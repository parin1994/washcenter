<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Carcare</title>
    <link href="img/favicon.png" rel="icon">
    <link href="<?php echo base_url('assets/services/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">
    <link href="<?php echo base_url('assets/services/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/services/lib/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/css/zabuto_calendar.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/lib/gritter/css/jquery.gritter.css') ?>" />
    <link href="<?php echo base_url('assets/services/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/services/css/style-responsive.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/services/lib/chart-master/Chart.js') ?>"></script>

 
</head>

<body>
    <section id="container">
        <?php echo $headers; ?>
        <?php echo $menu; ?>

        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9 main-chart">
                        <!--CUSTOM CHART START -->
                        <div class="border-head">
                            <h3><?php echo $title ?> <br></h3>
                            <p><a href="<?php echo base_url('carcareoffice/report') ?>" class="btn btn-default pull-right">
                                    <span class="fa fa-pencil">&nbsp;ออกรายงาน</span>
                                </a></p>
                        </div>
                        <br>
                        <br>
                        <div style="overflow-x:auto;">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:5%;text-align:center">คิว</th>
                                        <th style="width:10%;text-align:center">ชื่อ</th>
                                        <th style="width:10%;text-align:center">ยี่ห้อรถ</th>
                                        <th style="width:10%;text-align:center">ทะเบียนรถ</th>
                                        <th style="width:10%;text-align:center">ขนาดรถ</th>
                                        <th style="width:10%;text-align:center">โปรโมชัน</th>
                                        <th style="width:10%;text-align:center">ประเภทบริการ</th>
                                        <th style="width:10%;text-align:center">ราคา</th>
                                        <th style="width:10%;text-align:center">ยอดสุทธิ</th>
                                        <th style="width:10%;text-align:center">วันที่</th>
                                        <th style="width:10%;text-align:center">เวลา</th>
                                        <th style="width:10%;text-align:center">ใบเสร็จ</th>
                                        <th style="width:10%;text-align:center">แก้ไข</th>
                                        <th style="width:10%;text-align:center">ส่งข้อความ</th>
                                      
                                    </tr>


                                </thead>

                                <?php foreach ($read as $value) { ?>
                                    <tbody>
                                        <tr>

                                            <td style="width:5%;text-align:center"><?php echo $value->queue ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->name ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->name_car ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->car_number ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->type_car ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->name_promotion ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->option ?></td>
                                            <td style="width:10%;text-align:right"><?php echo $value->price ?>&nbsp;บาท</td>
                                            <td style="width:10%;text-align:right"><?php echo $value->total ?>&nbsp;บาท</td>
                                            <td style="width:10%;text-align:center"><?php echo date_format( new DateTime($value->date), 'd/m/Y' ) ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->time ?></td>
                                            <td style="width:30%;text-align:center"><img src="<?php echo base_url($value->img_receipt) ?>" class="thumbnail"style="height: 100px; width: 100px;"></td>

                                            <td style="width:10%;text-align:center"><a href="<?php echo base_url('carcareoffice/booking/update/' . $value->queue) ?>" id="edit" class="btn btn-primary"><span class="fa fa-edit"></span></a></td>
                                            <?php
                                              if ($value->status == 1) {
                                                        $color = "btn btn-warning";
                                                    } else {
                                                        $color = "btn btn-success";
                                                    }
                                                    ?>
                                            <td style="width:10%;text-align:center"><a href="<?php echo base_url('curl/index/' . $value->queue.'/'. $value->userid) ?>" class="<?php echo $color?>"><span class="fa fa-commenting-o"></span></a></td>

                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>

                </div>

            </section>
        </section>


        <!--footer end-->
    </section>

   
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/services/lib/jquery/jquery.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/services/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/services/lib/jquery.dcjqaccordion.2.7.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.scrollTo.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.nicescroll.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.sparkline.js') ?>"></script>
    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/services/lib/common-scripts.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter/js/jquery.gritter.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter-conf.js') ?>"></script>
    <!--script for this page-->
    <script src="<?php echo base_url('assets/services/lib/sparkline-chart.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/zabuto_calendar.js') ?>"></script>

    <script type="application/javascript">
        $(document).ready(function() {
            $("#date-popover").popover({
                html: true,
                trigger: "manual"
            });
            $("#date-popover").hide();
            $("#date-popover").click(function(e) {
                $(this).hide();
            });

            $("#my-calendar").zabuto_calendar({
                action: function() {
                    return myDateFunction(this.id, false);
                },
                action_nav: function() {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [{
                        type: "text",
                        label: "Special event",
                        badge: "00"
                    },
                    {
                        type: "block",
                        label: "Regular event",
                    }
                ]
            });
        });

        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
        if (window.self == window.top) {
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-55234356-6', 'auto');
            ga('send', 'pageview');
        }
    </script>

</body>

</html>