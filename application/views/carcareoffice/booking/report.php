<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Report</title>
    
    <link href="img/favicon.png" rel="icon">
    <link href="<?php echo base_url('assets/services/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">

    <link href="<?php echo base_url('assets/services/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/services/lib/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/css/zabuto_calendar.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/services/lib/gritter/css/jquery.gritter.css') ?>" />
    <link href="<?php echo base_url('assets/services/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/services/css/style-responsive.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/services/lib/chart-master/Chart.js') ?>"></script>


</head>

<body>
    <section id="container">

        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9 main-chart">
                        <!--CUSTOM CHART START -->
                        <div class="border-head">
                            <h3><?php echo $title ?> <br></h3>
                            <form action="<?php echo base_url('carcareoffice/report/search') ?>" method="post" enctype="multipart/form-data"> 
                                <input type="date" id="date" name="date">
                                <button type="submit"> ค้นหา </button>
                            </form>
                        </div>
                        <br>
                        <div style="overflow-x:auto;">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        ร้านบริการ : <?php echo $store->name_carcarestore ?>
                                    </div>
                                    <div class="col">
                                        วันที่ : <?php echo date_format(new DateTime($date), 'd/m/Y') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                    ที่อยู่ : <?php echo $store->address ?>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:10%;text-align:center">เวลา</th>
                                        <th style="width:10%;text-align:center">โปรโมชัน</th>
                                        <th style="width:20%;text-align:center">ประเภทบริการ</th>
                                        <th style="width:10%;text-align:center">ราคา</th>
                                    </tr>
                                </thead>

                                <?php foreach ($report as $value) { ?>
                                    <tbody>
                                        <tr>
                                            <td style="width:10%;text-align:center"><?php echo $value->time ?></td>
                                            <td style="width:10%;text-align:center"><?php echo $value->name_promotion ?></td>
                                            <td style="width:20%;text-align:center"><?php echo $value->option ?></td>
                                            <td style="width:10%;text-align:right"><?php echo $value->price ?>&nbsp;บาท</td>


                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                                <thead>
                                    <tr>
                                        <th style="width:10%;text-align:center"> </th>
                                        <th style="width:10%;text-align:center"></th>
                                        <th style="width:20%;text-align:center">ยอดสุทธิ </th>
                                        <th style="width:10%;text-align:right"> <?php echo $sum->total ?>&nbsp;บาท</th>
                                        
                                    </tr>
                                </thead>
                            </table>
                            
                        </div>
                    </div>
                </div>

                </div>

            </section>
        </section>


        <!--footer end-->
    </section>


    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/services/lib/jquery/jquery.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/services/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/services/lib/jquery.dcjqaccordion.2.7.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.scrollTo.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.nicescroll.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/services/lib/jquery.sparkline.js') ?>"></script>
    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/services/lib/common-scripts.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter/js/jquery.gritter.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/services/lib/gritter-conf.js') ?>"></script>
    <!--script for this page-->
    <script src="<?php echo base_url('assets/services/lib/sparkline-chart.js') ?>"></script>
    <script src="<?php echo base_url('assets/services/lib/zabuto_calendar.js') ?>"></script>


</body>

</html>