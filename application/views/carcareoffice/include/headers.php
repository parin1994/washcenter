<header class="header black-bg">
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars tooltips" data-placement="right"></div>
            </div>
            <!--logo start-->
            <a class="logo"><b style="font-size:18px">WASH<span> CENTER</span></b></a>
            <!--logo end-->
          
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="<?php echo base_url('carcareoffice/login/logout')?>">ออกจากระบบ</a></li>
                </ul>
            </div>
        </header>