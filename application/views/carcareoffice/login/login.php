<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="<?php echo base_url('assets/login/images/icons/favicon.ico') ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/vendor/bootstrap/css/bootstrap.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/fonts/iconic/css/material-design-iconic-font.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/vendor/animate/animate.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/vendor/css-hamburgers/hamburgers.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/vendor/animsition/css/animsition.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/vendor/select2/select2.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/vendor/daterangepicker/daterangepicker.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/css/util.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/login/css/main.css') ?>">

    <style>
        img {
            border-radius: 100 px
        }
    </style>
</head>

<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url(<?php echo base_url('assets/login/images/bg-01.jpg') ?>);">
            <div class="wrap-login100">
                <form role="form" action="<?php echo base_url('carcareoffice/login/auth') ?>" method="post" enctype="multipart/form-data">
                    <span class="login100-form-logo">
                        <img src='http://carcare/assets/login/images/car.png' radius="50 px" width="200 px" height="130 px"></i>
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27">

                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Enter username">
                        <input class="input100" type="text" id="email" name="email" placeholder="อีเมล">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="pass" placeholder="รหัสผ่าน">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            เข้าสู่ระบบ
                        </button>
                    </div>
                </form>
                <br>

                <div class="container-login100-form-btn">
                    <a class="login100-form-btn" href="<?php echo base_url('carcarestore/index') ?>">
                        สมัครสมาชิกร้านค้า
                    </a>
                </div>


            </div>
        </div>
    </div>


    <script src="<?php echo base_url('assets/login/vendor/jquery/jquery-3.2.1.min.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script src="<?php echo base_url('assets/login/vendor/animsition/js/animsition.min.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script src="<?php echo base_url('assets/login/vendor/bootstrap/js/popper.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>
    <script src="<?php echo base_url('assets/login/vendor/bootstrap/js/bootstrap.min.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script src="<?php echo base_url('assets/login/vendor/select2/select2.min.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script src="<?php echo base_url('assets/login/vendor/daterangepicker/moment.min.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>
    <script src="<?php echo base_url('assets/login/vendor/daterangepicker/daterangepicker.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script src="<?php echo base_url('assets/login/vendor/countdowntime/countdowntime.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script src="<?php echo base_url('assets/login/js/main.js') ?>" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="5b4fa66b30aca65fedbebbfa-text/javascript"></script>
    <script type="5b4fa66b30aca65fedbebbfa-text/javascript">
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="5b4fa66b30aca65fedbebbfa-|49" defer=""></script>

</body>

</html>