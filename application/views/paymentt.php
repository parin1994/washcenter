<!DOCTYPE html>
<html lang="en">
<script src="https://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/vconsole/3.0.0/vconsole.min.js"></script>
<script>
    var vConsole = new VConsole();
</script>

<head>
    <title>Payment</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/register/images/icons/favicon.ico') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/bootstrap/css/bootstrap.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/animate/animate.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/css-hamburgers/hamburgers.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/animsition/css/animsition.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/select2/select2.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/vendor/daterangepicker/daterangepicker.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/css/util.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/register/css/main.css') ?>">



</head>
<style>
    .js-select2 {
        font-family: Poppins-Regular;
        font-size: 15px;
        color: #555555;
        text-transform: uppercase;
        letter-spacing: 1px;

        width: 450%;
        min-height: 55px;
        border: 1px solid #e6e6e6;
        margin-top: 0px;
        line-height: 1.2;
        padding: 0 25px;

        margin-bottom: 0;
    }

    .label-input150 {
        align-items: center;
        font-family: Poppins-Regular;
        font-size: 12px;
        color: #555555;
        line-height: 1.5;
        text-transform: uppercase;
        letter-spacing: 1px;

        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;

        width: 100%;
        min-height: 55px;
        border: 1px solid #e6e6e6;
        padding: 10px 25px;
        margin-top: 15px;
        margin-bottom: 0;
    }
</style>

<body>
    <div class="container-contact100" style="background-color:white;">
        <div class="card col-12 col-md-6 offset-md-2">

            <form action="<?php echo base_url('promotion/payment') ?>" method="post" class="contact100-form validate-form" style="width:100%;" enctype="multipart/form-data">
            <input type="hidden" id="userid" name="userid" value="<?php echo $payment->userid ?>">
            <input type="hidden" id="name" name="name" value="<?php echo $payment->name ?>">
            <input type="hidden" id="last_name" name="last_name" value="<?php echo $payment->last_name ?>">
            <input type="hidden" id="name_carcarestore" name="name_carcarestore" value="<?php echo $payment->name_carcarestore ?>">
            <input type="hidden" id="name_car" name="name_car" value="<?php echo $payment->name_car ?>">
            <input type="hidden" id="car_number" name="car_number" value="<?php echo $payment->car_number ?>">
            <input type="hidden" id="type_car" name="type_car" value="<?php echo $payment->type_car ?>">
            <input type="hidden" id="name_promotion" name="name_promotion" value="<?php echo $payment->name_promotion ?>">
            <input type="hidden" id="option" name="option" value="<?php echo $payment->option ?>">
            <input type="hidden" id="price" name="price" value="<?php echo $payment->price ?>">
            <input type="hidden" id="total" name="total" value="<?php echo $payment->total ?>">
            <input type="hidden" id="time" name="time" value="<?php echo $payment->time ?>">
            <input type="hidden" id="id_date" name="id_date" value="<?php echo $payment->id_date ?>">
            <input type="hidden" id="date" name="date" value="<?php echo $payment->date ?>">
            <input type="hidden" id="id_receipt" name="id_receipt" value="<?php echo $payment->id_receipt ?>">
            <input type="hidden" id="status" name="status" value="<?php echo $payment->status ?>">


                <span class="col-12 text-center">
                    <h5>ช่องทางการชำระเงิน</h5>
                    <br>
                    <?php foreach ($read as $value) { ?>
                        <img src="<?php echo base_url($value->img_bank) ?>" class="thumbnail" style="height: 150px; width: 150px;">
                        <br><br>
                        <?php echo $value->bank_number ?>
                        <br>
                        <?php echo $value->bank_bank ?>
                        <br>
                        <?php echo $value->name_bank ?>
                    <?php } ?>
                    <br><br>
                    <hr width=100% size="50" color=770088>
                    </hr>
                    <h6><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><i class="fa fa-stumbleupon" aria-hidden="true"></i><h6>
                    <hr  width=100% size=10 color=770088>
                    </hr>
                    <br><br>
                    <h5>ชำระเงิน</h5>
                    <br>
                </span>
                <?php echo $payment->id_receipt ?>
                <h6>คุณ<?php echo $payment->name ?>
                <?php echo $payment->last_name ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                เวลาที่จอง : <?php echo $payment->time?></h6>
                <br>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            ยี่ห้อรถ
                        </div>
                        <div class="col-4">
                            ขนาดรถ
                        </div>
                        <div class="col-4">
                            ทะเบียน
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <?php echo $payment->name_car ?>
                        </div>
                        <div class="col-4">
                            <?php echo $payment->car_number ?>
                        </div>
                        <div class="col-4">
                            <?php echo $payment->type_car ?>
                        </div>
                    </div>
                </div>
                <hr noshade="noshade" width=120% size=5 >
                </hr>
                <div class="container">
                <div class="row">
                    <div class="col-8">ประเภทการล้าง</div>
                    <div class="col-4">ราคา</div>
                </div>
                </div>
                <div class="container">
                <div class="row">
                    <div class="col-12">โปรโมชัน : <?php echo $payment->name_promotion ?></div>
                </div>
                </div>
                <div class="container">
                <div class="row">
                    <div class="col-8"><?php echo $payment->option ?></div>
                    <div class="col-4"><?php echo $payment->price ?>&nbsp;&nbsp;บาท</div>
                </div>
                </div>

                <hr noshade="noshade" width=120% size=5 >
                </hr>
                <div class="container">
                <div class="row">
                    <div class="col-8">ยอดสุทธิ</div>
                    <div class="col-4"><?php echo $payment->price ?>&nbsp;&nbsp;บาท</div>
                </div>
                </div>
                <hr  noshade="noshade"width=120% size=5 >
                </hr>
                <br><br><br>
                <div class="container">
                <span class="col-12 text-center">
                <h5>อัพโหลดใบเสร็จการชำระเงิน</h5>
                <br>
                <input type="file" name="img" id="img" >
                </span>
                </div>
                <br>
                <div class="container-contact100-form-btn">
                    <button  class="contact100-form-btn">
                        ยืนยัน
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div id="dropDownSelect1"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
    <script src="<?php echo base_url('assets/register/vendor/jquery/jquery-3.2.1.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/animsition/js/animsition.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/bootstrap/js/popper.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script src="<?php echo base_url('assets/register/vendor/bootstrap/js/bootstrap.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/select2/select2.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script type="3d44b465189b22b734a3929d-text/javascript">
        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>

    <script src="<?php echo base_url('assets/register/vendor/daterangepicker/moment.min.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script src="<?php echo base_url('assets/register/vendor/daterangepicker/daterangepicker.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/vendor/countdowntime/countdowntime.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script src="<?php echo base_url('assets/register/js/main.js') ?>" type="3d44b465189b22b734a3929d-text/javascript"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="3d44b465189b22b734a3929d-text/javascript"></script>
    <script type="3d44b465189b22b734a3929d-text/javascript">
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="3d44b465189b22b734a3929d-|49" defer=""></script>
</body>
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
<script src="liff-starter.js"></script>

</html>